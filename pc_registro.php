<?php include('./header.php'); ?>  
<hr/>
<div class="container">
   <div class="row c_form" id="contact_header">
      <div class="title-contact">
         <h1>Registrate</h1>
      </div>
   </div>
   <div class="row c_form" id="contactForm">
      <form action="#" id="register" method="post">
         <div class="col-md-12 col-xs-12">
            <h2 class="legend">Información de Contacto</h2>
         </div>
         <div class="col-md-6 col-xs-12">
            <label for="name" class="required"><em>*</em>Sexo</label>
            <select name="subject" id="subject_a" class="required-entry">
               <option value="" disabled="" selected="">Seleccionar. . .</option>
               <option value="#"> Masculino </option>
               <option value="#"> Femenino </option>
            </select>
         </div>
         <div class="col-md-6 col-xs-12">
            <label for="email" class="required"><em>*</em> E-mail</label> 
            <input type="text" id="type_10" class="form-control" placeholder="">
         </div>
         <div class="col-md-6 col-xs-12">
            <label for="name" class="required"><em>*</em>Nombre</label>
            <input type="text" id="type_10" class="form-control" placeholder="">
         </div>
         <div class="col-md-6 col-xs-12">
            <label for="email" class="required"><em>*</em>Apellido</label> 
            <input type="text" id="type_10" class="form-control" placeholder="">
         </div>
         <div class="col-md-12 col-xs-12" id="fecha_cumple">
            <label for="email" class="required"><em>*</em>Fecha de Nacimiento</label> 
            <div>
               <div class="w25"> 
                  <input type="text" class="form-control" placeholder="DD"> 
               </div>
               <div class="w25"> 
                  <input type="text" class="form-control" placeholder="MM"> 
               </div>
               <div class="w50"> 
                  <input type="text" class="form-control" placeholder="AAAA"> 
               </div>
            </div>
         </div>
         <div class="col-md-12 col-xs-12">
            <h2 class="legend"> INFORMACIÓN DE INICIO DE SESIÓN </h2>
         </div>
         <div class="col-md-6 col-xs-12">
            <label for="email" class="required"><em>*</em>Contraseña</label>
            <input type="password" id="type_10" class="form-control" placeholder="">
         </div>
         <div class="col-md-6 col-xs-12">
            <label for="email" class="required"><em>*</em>Confirmar la contraseña</label>
            <input type="password" id="type_10" class="form-control" placeholder="">
         </div>
         <div class="col-md-12 col-xs-12">
            <p class="det_camp"> * Campos Obligatorios </p>
            <button type="submit" title="GUARDAR" class="button send_t"><span>GUARDAR</span></button>  
         </div>
      </form>
   </div>
</div>
<?php include ('./footer.php'); ?>