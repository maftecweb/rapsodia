<?php include ('./header.php'); ?> 
<hr>
<div class="container">
   <div class="col-md-2 detail_account">
      <h4 class="my_account"> Mi cuenta </h4>
      <hr class="d_hr">
      <ul>
         <li>Mis pedidos</li>
         <li>Mis devoluciones</li>
         <li>Mis cupones</li>
         <li>Información de cuenta</li>
         <li>Libreta de direcciones</li>
         <li>Mis Tarjetas</li>
         <li>Favoritos</li>
         <li>Notificaciones via Email?</li>
      </ul>
   </div>
   <div class="col-md-10 big_panel">
      <div class="row">
         <!-- / BIG PEDIDOS -->
         <div class="big_pedidos">
            <h2> Mis Pedidos </h2>
            <hr>
            <!-- Pedidos lleno -->
            <div class="pedidos_mostrar">
               <div class="l-cell txtRight">
                  Mostrar        
                  <select class="pager-select">
                     <option selected="selected" value="https://www.promoda.com.mx/customer/order/index/?limit=10">10</option>
                     <option value="https://www.promoda.com.mx/customer/order/index/?limit=20">20</option>
                     <option value="https://www.promoda.com.mx/customer/order/index/?limit=50">50</option>
                  </select>
                  Página    
               </div>
               <div class="l-cell txtRight"> 0 Ordenes en los últimos 90 días </div>
               <div class="table-responsive">
                  <table class="table">
                     <thead>
                        <tr>
                           <th>Fecha</th>
                           <th>Pedido #</th>
                           <th>Dirección</th>
                           <th>Total</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>04/06/16</td>
                           <td>XYZSNES9091</td>
                           <td>Nuevo León</td>
                           <td>$792.00</td>
                        </tr>
                     </tbody>
                  </table>
                  <div class="l-cell txtRight">
                     Mostrar        
                     <select class="pager-select">
                        <option selected="selected" value="https://www.promoda.com.mx/customer/order/index/?limit=10">10</option>
                        <option value="https://www.promoda.com.mx/customer/order/index/?limit=20">20</option>
                        <option value="https://www.promoda.com.mx/customer/order/index/?limit=50">50</option>
                     </select>
                     Página    
                  </div>
               </div>
            </div>
            <!-- Pedidos lleno --> 
            <!-- Pedidos vacio -->
            <div class="pedidos_vacio">
               <div class="big_marg no_content">
                  <p>Usted no ha realizado Pedidos</p>
                  <p>Esta función no está disponible en la versión móvil</p>
               </div>
            </div>
            <!-- /Pedidos vacio --> 
         </div>
         <!-- / BIG PEDIDOS -->
         <!-- / BIG DEVOLUCIONES -->
         <div class="big_pedidos">
            <h2> Mis Devoluciones </h2>
            <hr>
            <!-- Devoluciones mostrar --> 
            <div class="devoluciones_mostrar">
               <div class="l-cell txtRight">
                  Mostrar        
                  <select class="pager-select">
                     <option selected="selected" value="https://www.promoda.com.mx/customer/order/index/?limit=10">10</option>
                     <option value="https://www.promoda.com.mx/customer/order/index/?limit=20">20</option>
                     <option value="https://www.promoda.com.mx/customer/order/index/?limit=50">50</option>
                  </select>
                  Página    
               </div>
               <div class="l-cell txtRight"> 0 Ordenes en los últimos 90 días </div>
               <div class="table-responsive">
                  <table class="table">
                     <thead>
                        <tr>
                           <th>Pedido #</th>
                           <th>SKU</th>
                           <th>Fecha de devolucion</th>
                           <th>Motivo</th>
                           <th>Codigo de rastreo</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>740</td>
                           <td>XYZSNES9091</td>
                           <td>4/08/2015</td>
                           <td>Prenda equivocada</td>
                           <td>74899992554340743694</td>
                        </tr>
                     </tbody>
                  </table>
                  <div class="l-cell txtRight">
                     Mostrar        
                     <select class="pager-select">
                        <option selected="selected" value="https://www.promoda.com.mx/customer/order/index/?limit=10">10</option>
                        <option value="https://www.promoda.com.mx/customer/order/index/?limit=20">20</option>
                        <option value="https://www.promoda.com.mx/customer/order/index/?limit=50">50</option>
                     </select>
                     Página    
                  </div>
               </div>
            </div>
            <!-- Devoluciones mostrar --> 
            <!-- Devoluciones vacio -->
            <div class="pedidos_vacio">
               <div class="big_marg no_content">
                  <p>No tienes ordenes devueltas</p>
                  <p>Esta función no está disponible en la versión móvil</p>
               </div>
            </div>
            <!-- /Devoluciones vacio --> 
         </div>
         <!-- / BIG DEVOLUCIONES -->
         <!-- / BIG CUPONES -->
         <div class="big_pedidos">
            <h2> Mis Cupones </h2>
            <hr>
            <!-- Cupones mostrar -->
            <div class="cupones_mostrar">
               <div class="l-cell txtRight">
                  Mostrar        
                  <select class="pager-select">
                     <option selected="selected" value="https://www.promoda.com.mx/customer/order/index/?limit=10">10</option>
                     <option value="https://www.promoda.com.mx/customer/order/index/?limit=20">20</option>
                     <option value="https://www.promoda.com.mx/customer/order/index/?limit=50">50</option>
                  </select>
                  Página    
               </div>
               <div class="l-cell txtRight"> 0 Ordenes en los últimos 90 días </div>
               <div class="table-responsive">
                  <table class="table">
                     <thead>
                        <tr>
                           <th>Clave cupon</th>
                           <th>Fecha de expiracion</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>MAY0500DESCUENTO</td>
                           <td>31/05/2016</td>
                        </tr>
                     </tbody>
                  </table>
                  <div class="l-cell txtRight">
                     Mostrar        
                     <select class="pager-select">
                        <option selected="selected" value="https://www.promoda.com.mx/customer/order/index/?limit=10">10</option>
                        <option value="https://www.promoda.com.mx/customer/order/index/?limit=20">20</option>
                        <option value="https://www.promoda.com.mx/customer/order/index/?limit=50">50</option>
                     </select>
                     Página    
                  </div>
               </div>
            </div>
            <!--  /Cupones mostrar -->
            <!-- Devoluciones vacio -->
            <div class="pedidos_vacio">
               <div class="big_marg no_content">
                  <p>No tienes cupones</p>
               </div>
            </div>
            <!-- /Devoluciones vacio --> 
         </div>
         <!-- / BIG CUPONES -->
         <div class="big_pedidos">
            <h2> Editar Cuenta </h2>
            <hr>
            <!-- FORMA -->
            <div class="row c_form" id="edit_account">
               <form action="#" id="register" method="post">
                  <div class="col-md-12 col-xs-12">
                     <h2 class="legend"> INFORMACIÓN DE LA CUENTA </h2>
                  </div>
                  <div class="col-md-6 col-xs-12">
                     <label for="name" class="required"><em>*</em>Sexo</label>
                     <select name="subject" id="subject_a" class="required-entry">
                        <option value="" disabled="" selected="">Seleccionar. . .</option>
                        <option value="#"> Masculino </option>
                        <option value="#"> Femenino </option>
                     </select>
                  </div>
                  <div class="col-md-6 col-xs-12">
                     <label for="email" class="required"><em>*</em> E-mail</label> 
                     <input type="text" id="type_10" class="form-control" placeholder="">
                  </div>
                  <div class="col-md-6 col-xs-12">
                     <label for="name" class="required"><em>*</em>Nombre</label>
                     <input type="text" id="type_10" class="form-control" placeholder="">
                  </div>
                  <div class="col-md-6 col-xs-12">
                     <label for="email" class="required"><em>*</em>Apellido</label> 
                     <input type="text" id="type_10" class="form-control" placeholder="">
                  </div>
                  <div class="col-md-12 col-xs-12" id="fecha_cumple">
                     <label for="email" class="required"><em>*</em>Fecha de Nacimiento</label> 
                     <div>
                        <div class="w25"> 
                           <input type="text" class="form-control" placeholder="DD"> 
                        </div>
                        <div class="w25"> 
                           <input type="text" class="form-control" placeholder="MM"> 
                        </div>
                        <div class="w50"> 
                           <input type="text" class="form-control" placeholder="AAAA"> 
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12 col-xs-12">
                     <p class="det_camp"> * Campos Obligatorios </p>
                     <p class="det_camp"> < Volver </p>
                     <button type="submit" title="GUARDAR" class="button send_t"><span>GUARDAR</span></button>  
                  </div>
               </form>
            </div>
         </div>
         <!-- /FORMA -->
         <!-- FORMA LLENA -->
         <!-- FORMA LLENA -->
         <div class="row c_form" id="edit_account">
            <form action="#" id="register" method="post">
               <div class="col-md-12 col-xs-12">
                  <h2 class="legend"> INFORMACIÓN DE LA CUENTA </h2>
               </div>
               <div class="col-md-6 col-xs-12">
                  <label for="name" class="required"><em>*</em>Sexo</label>
                  <p> Masculino </p>
               </div>
               <div class="col-md-6 col-xs-12">
                  <label for="email" class="required"><em>*</em> E-mail</label> 
                  <p>ernesto@mail.com</p>
               </div>
               <div class="col-md-6 col-xs-12">
                  <label for="name" class="required"><em>*</em>Nombre</label>
                  <p> Ernesto Cárdenas </p>
               </div>
               <div class="col-md-6 col-xs-12" id="fecha_cumple">
                  <label for="email" class="required"><em>*</em>Fecha de Nacimiento</label> 
                  <p> 15 / 10 / 1990  </p>
                  <div>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12">
                  <p class="det_camp"> < Volver </p>
                  <button type="submit" title="GUARDAR" class="button send_t"><span>EDITAR</span></button>  
               </div>
            </form>
         </div>
         <!-- FORMA LLENA -->
         <br/> <br/> 
         <!-- / BIG PEDIDOS -->
         <div class="big_pedidos">
            <h2> Mis Favoritos </h2>
            <hr>
            <!-- Pedidos lleno -->
            <div class="pedidos_mostrar">
               <div class="row">
                  <div class="col-xs-12 col-sm-6 col-md-3">
                     <div class="over-detail">
                        <img src="./media/catalog/01.jpg"> 
                        <div class="info-producto">
                           <h3><a href="#" title="#">Top Maggie Sari</a></h3>
                           <div class="over_price"> 
                              <span class="price">$1.400</span>                              
                           </div>
                           <div class="over_more">
                              <div class="#">
                                 <dl>
                                    <dt>Color: </dt>
                                    <dd>Multicolor</dd>
                                 </dl>
                              </div>
                              <div class="#">
                                 <dl>
                                    <dt>Cantidad: </dt>
                                    <dd>1</dd>
                                 </dl>
                              </div>
                           </div>
                           <br/>
                           <a href="#">Editar</a> 
                           <br/>
                           <a href="#" title="Eliminar" class="btn-remove btn-remove">Eliminar</a>
                        </div>
                     </div>
                     <h3><a href="#"> Añadir al carrito </a> </h3>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-3">
                     <div class="over-detail">
                        <img src="./media/catalog/01.jpg"> 
                        <div class="info-producto">
                           <h3><a href="#" title="#">Top Maggie Sari</a></h3>
                           <div class="over_price"> 
                              <span class="price">$1.400</span>                              
                           </div>
                           <div class="over_more">
                              <div class="#">
                                 <dl>
                                    <dt>Color: </dt>
                                    <dd>Multicolor</dd>
                                 </dl>
                              </div>
                              <div class="#">
                                 <dl>
                                    <dt>Cantidad: </dt>
                                    <dd>1</dd>
                                 </dl>
                              </div>
                           </div>
                           <br/>
                           <a href="#">Editar</a> 
                           <br/>
                           <a href="#" title="Eliminar" class="btn-remove btn-remove">Eliminar</a>
                        </div>
                     </div>
                     <h3><a href="#"> Añadir al carrito </a> </h3>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-3">
                     <div class="over-detail">
                        <img src="./media/catalog/01.jpg"> 
                        <div class="info-producto">
                           <h3><a href="#" title="#">Top Maggie Sari</a></h3>
                           <div class="over_price"> 
                              <span class="price">$1.400</span>                              
                           </div>
                           <div class="over_more">
                              <div class="#">
                                 <dl>
                                    <dt>Color: </dt>
                                    <dd>Multicolor</dd>
                                 </dl>
                              </div>
                              <div class="#">
                                 <dl>
                                    <dt>Cantidad: </dt>
                                    <dd>1</dd>
                                 </dl>
                              </div>
                           </div>
                           <br/>
                           <a href="#">Editar</a> 
                           <br/>
                           <a href="#" title="Eliminar" class="btn-remove btn-remove">Eliminar</a>
                        </div>
                     </div>
                     <h3><a href="#"> Añadir al carrito </a> </h3>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-3">
                     <div class="over-detail">
                        <img src="./media/catalog/01.jpg"> 
                        <div class="info-producto">
                           <h3><a href="#" title="#">Top Maggie Sari</a></h3>
                           <div class="over_price"> 
                              <span class="price">$1.400</span>                              
                           </div>
                           <div class="over_more">
                              <div class="#">
                                 <dl>
                                    <dt>Color: </dt>
                                    <dd>Multicolor</dd>
                                 </dl>
                              </div>
                              <div class="#">
                                 <dl>
                                    <dt>Cantidad: </dt>
                                    <dd>1</dd>
                                 </dl>
                              </div>
                           </div>
                           <br/>
                           <a href="#">Editar</a> 
                           <br/>
                           <a href="#" title="Eliminar" class="btn-remove btn-remove">Eliminar</a>
                        </div>
                     </div>
                     <h3><a href="#"> Añadir al carrito </a> </h3>
                  </div>
               </div>
            </div>
            <!-- Pedidos lleno --> 
         </div>
         <!-- / BIG PEDIDOS -->
         <!-- / BIG TARJETAS -->
         <div class="big_pedidos">
            <h2> Mis Tarjetas </h2>
            <hr>
            <!-- TARJETAS mostrar --> 
            <div class="devoluciones_mostrar">
               <div class="l-cell txtRight">
                  Mostrar        
                  <select class="pager-select">
                     <option selected="selected" value="https://www.promoda.com.mx/customer/order/index/?limit=10">10</option>
                     <option value="https://www.promoda.com.mx/customer/order/index/?limit=20">20</option>
                     <option value="https://www.promoda.com.mx/customer/order/index/?limit=50">50</option>
                  </select>
                  Página    
               </div>
               <div class="l-cell txtRight"> 0 Ordenes en los últimos 90 días </div>
               <div class="table-responsive">
                  <table class="table">
                     <thead>
                        <tr>
                           <th>Tarjeta</th>
                           <th>Terminación</th>
                           <th>Compras</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>VISA</td>
                           <td>.... ....... 404</td>
                           <td>4</td>
                        </tr>
                     </tbody>
                  </table>
                  <div class="l-cell txtRight">
                     Mostrar        
                     <select class="pager-select">
                        <option selected="selected" value="https://www.promoda.com.mx/customer/order/index/?limit=10">10</option>
                        <option value="https://www.promoda.com.mx/customer/order/index/?limit=20">20</option>
                        <option value="https://www.promoda.com.mx/customer/order/index/?limit=50">50</option>
                     </select>
                     Página    
                  </div>
               </div>
            </div>
            <!-- TARJETAS mostrar --> 
            <!-- TARJETAS vacio -->
            <div class="pedidos_vacio">
               <div class="big_marg no_content">
                  <p>No tienes tarjetas</p>
               </div>
            </div>
            <!-- /TARJETAS vacio --> 
         </div>
         <!-- / BIG TARJETAS -->
         <!-- / BIG newsletters -->
         <div class="big_pedidos">
            <h2> Administrar newsletters </h2>
            <hr>
            <!-- newsletters mostrar --> 
            <div class="devoluciones_mostrar">
               <div class="select_news">
                  <span>
                  <input  style="margin-right: 5px;" id="#" value="#" name="#" type="checkbox">
                  <label class="inline ui-inputCheckboxLabel" for="#">Newsletter</label>
                  </span>
                  <br>
                  <span>
                  <input  style="margin-right: 5px;" id="#" value="#" name="#" type="checkbox">
                  <label class="inline ui-inputCheckboxLabel" for="#">Notificaciones De  Marketing</label>
                  </span>
                  <br>
               </div>
            </div>
            <!-- newsletters mostrar --> 
         </div>
         <!-- / BIG newsletters -->


<!-- libreta -->
<div class="big_pedidos">
   <h2> Libreta de direcciones </h2>
   <hr>
   <div class="row c_form" id="edit_account">
      <form action="#" id="register" method="post">
         <div class="col-md-12 col-xs-12">
            <h2 class="legend"> DATOS DE CONTACTO </h2>
         </div>
         <div class="col-md-6 col-xs-12">
            <label for="name" class="required"><em>*</em>Nombre</label>
            <input type="text" id="type_10" class="form-control" placeholder="">
         </div>
         <div class="col-md-6 col-xs-12">
            <label for="name" class="required"><em>*</em>Apellido</label>
            <input type="text" id="type_10" class="form-control" placeholder="">
         </div>
         <div class="col-md-6 col-xs-12">
            <label for="name" class="required"><em>*</em>DNI</label>
            <input type="text" id="type_10" class="form-control" placeholder="">
         </div>
         <div class="col-md-6 col-xs-12">
            <label for="name" class="required"><em>*</em>Celular</label>
            <input type="text" id="type_10" class="form-control" placeholder="">
         </div>
         <div class="col-md-12 col-xs-12">
            <h2 class="legend dir_title"> DIRECCIÓN </h2>
         </div>
         <div class="col-md-6 col-xs-12">
            <label for="name" class="required"><em>*</em>Estado/Provinncia </label>
            <select name="subject" id="subject_a" class="required-entry">
               <option value="" disabled="" selected="">Por favor seleccione una región, estado o una provincia</option>
               <option value="#"> Aguascalientes</option>
               <option value="#"> Baja California Norte</option>
               <option value="#"> Baja California Sur</option>
               <option value="#"> Campeche</option>
               <option value="#"> Ciudad de Mèxico</option>
               <option value="#">Chiapas</option>
               <option value="#"> Coahuila</option>
               <option value="#"> Colima</option>
            </select>
         </div>
         <div class="col-md-6 col-xs-12">
            <label for="name" class="required"><em>*</em>Ciudad</label>
            <input type="text" id="type_10" class="form-control" placeholder="">
         </div>
         <div class="col-md-6 col-xs-12">
            <label for="name" class="required"><em>*</em>Còdigo Postal</label>
            <input type="text" id="type_10" class="form-control" placeholder="">
         </div>
         <div class="col-md-6 col-xs-12">
            <label for="name" class="required"><em>*</em>Calle y altura</label>
            <input type="text" id="type_10" class="form-control" placeholder="">
         </div>
         <div class="col-md-12 col-xs-12">
            <p class="det_camp"> * Campos Obligatorios </p>
            <p class="det_camp"> < Volver </p>
            <button type="submit" title="GUARDAR" class="button send_t"><span>GUARDAR</span></button>  
         </div>
      </form>
   </div>
</div>
<!--BIG libreta  -->








<!-- libreta -->
<div class="big_pedidos">
   <h2> Mis Consultas </h2>
   <hr>
   <div class="row c_form" id="edit_account">
      <form action="#" id="register" method="post">
         <div class="col-md-12 col-xs-12">

            <p>No hay consultas enviadas</p>
            <h2 class="legend"> CREAR NUEVA CONSULTA </h2>
         </div>

         <div class="col-md-6 col-xs-12">
            <label for="name" class="required"><em>*</em>Titulo</label>
            <input type="text" id="type_10" class="form-control" placeholder="">
         </div>
         
    <div class="col-md-12 col-xs-12"> </div>

         <div class="col-md-6 col-xs-12">
            <label for="name" class="required"><em>*</em> Mensaje </label>

             <textarea class="form-control" rows="10" id="mensaje_consulta"></textarea>
           
         </div>
        
         <div class="col-md-12 col-xs-12">
            <p class="det_camp"> * Campos Obligatorios </p>
            <p class="det_camp"> < Volver </p>
            <button type="submit" title="GUARDAR" class="button send_t"><span>ENVIAR CONSULTA</span></button>  
         </div>


      </form>
   </div>
</div>
<!--BIG libreta  -->





         <!--  BIG direcccions  --> 
         <div class="big_pedidos">
            <h2> Libreta de direcciones </h2>
            <hr>
            <!-- direcciones mostrar --> 
            <div class="row" id="d_direccion">
               <div class="col-md-6">
                  <div class="brd">
                     <h4> Dirección por defecto </h4>
                     <p class="p_title">Dirección de Facturación</p>
                     <table>
                        <tbody>
                           <tr>
                              <td class="address-title">Nombre</td>
                              <td><span class="firstname">Ernesto</span> <span class="lastname">Cardenas</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Dirección</td>
                              <td><span class="street">ND</span> <span class="exterior">140</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Colonia</td>
                              <td><span class="neighborhood">CRISTO REY</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Estado</td>
                              <td><span class="city">ALVARO OBREGON</span>, <span class="state">Distrito Federal</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Código postal</td>
                              <td><span class="postcode">01150</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Teléfono</td>
                              <td><span class="phone">5551044957</span></td>
                           </tr>
                        </tbody>
                     </table>
                     <a class="direction df" href="#"> Editar dirección </a>
                     <hr/>
                     <p class="p_title">Dirección de Envío</p>
                     <table>
                        <tbody>
                           <tr>
                              <td class="address-title">Nombre</td>
                              <td><span class="firstname">Ernesto</span> <span class="lastname">Cardenas</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Dirección</td>
                              <td><span class="street">ND</span> <span class="exterior">140</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Colonia</td>
                              <td><span class="neighborhood">CRISTO REY</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Estado</td>
                              <td><span class="city">ALVARO OBREGON</span>, <span class="state">Distrito Federal</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Código postal</td>
                              <td><span class="postcode">01150</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Teléfono</td>
                              <td><span class="phone">5551044957</span></td>
                           </tr>
                        </tbody>
                     </table>
                     <p class="i-information"> Esta es tu dirección de facturación  </p>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="brd">
                     <h4> Direcciones adicionales </h4>
                     <p class="p_title">Dirección de Facturación</p>
                     <table>
                        <tbody>
                           <tr>
                              <td class="address-title">Nombre</td>
                              <td><span class="firstname">Ernesto</span> <span class="lastname">Cardenas</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Dirección</td>
                              <td><span class="street">ND</span> <span class="exterior">140</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Colonia</td>
                              <td><span class="neighborhood">CRISTO REY</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Estado</td>
                              <td><span class="city">ALVARO OBREGON</span>, <span class="state">Distrito Federal</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Código postal</td>
                              <td><span class="postcode">01150</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Teléfono</td>
                              <td><span class="phone">5551044957</span></td>
                           </tr>
                        </tbody>
                     </table>
                     <a class="direction df" href="#"> Editar dirección </a>
                     <a class="direction rd" href="#"> Borrar dirección </a>
                     <a class="direction pr" href="#"> Establecer como predeterminada para envío </a>
                     <hr/>
                     <p class="p_title">Dirección de Envío</p>
                     <table>
                        <tbody>
                           <tr>
                              <td class="address-title">Nombre</td>
                              <td><span class="firstname">Ernesto</span> <span class="lastname">Cardenas</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Dirección</td>
                              <td><span class="street">ND</span> <span class="exterior">140</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Colonia</td>
                              <td><span class="neighborhood">CRISTO REY</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Estado</td>
                              <td><span class="city">ALVARO OBREGON</span>, <span class="state">Distrito Federal</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Código postal</td>
                              <td><span class="postcode">01150</span></td>
                           </tr>
                           <tr>
                              <td class="address-title">Teléfono</td>
                              <td><span class="phone">5551044957</span></td>
                           </tr>
                        </tbody>
                     </table>
                     <a class="direction df" href="#"> Editar dirección </a>
                     <a class="direction rd" href="#"> Borrar dirección </a>
                     <a class="direction pr" href="#"> Establecer como predeterminada para envío </a>
                  </div>
               </div>
            </div>
            <!-- direcciones mostrar --> 
         </div>
         <!-- / BIG newsletters -->




         <!--  BIG direcccions  --> 
         <div class="big_pedidos">
            

            <h2 class="libreta"> Pedido #030202 de 17/02/16 10:33 </h2>


            <hr>
            <!-- direcciones mostrar --> 
            <div class="row" id="d_direccion">
               
<div class="b-green"> 

<div class="col-md-3 col-green col-step bck"> <h1>1</h1> <p>Pedido Recibido</p> 
<img src="./media/default/md.png"> 
</div>

<div class="col-md-3 col-green col-step bck"> <h1>2</h1> <p>Pago Recibido</p> 
<img src="./media/default/md.png"> 
</div>
<div class="col-md-3 col-green col-step bck"> <h1>3</h1> <p>Pedido Enviado</p> 
<img src="./media/default/md.png"> 
</div>
<div class="col-md-3 col-gray col-step fin"> <h1>4</h1> <p>Entrega Pendiente</p> 
</div>
</div>








 
<div class="row">
<div class="col-md-6"> 

<h4> Dirección de Facturación </h4>


<table class="table" id="tabla-shop">
    <thead>

    </thead>
    <tbody>
      <tr>
        <td><b>Nombre</b></td>
        <td>Ernesto</td>
 
      </tr>

      <tr>
        <td> <b> Dirección </b> </td>
        <td>Arte 222</td>
 
      </tr>

      <tr>
        <td> <b> Colonia </b> </td>
        <td>Aragon la Villa</td>
  
      </tr>

      <tr>
        <td> <b> Estado </b> </td>
        <td>D.F</td>
  
      </tr>

      <tr>
        <td> <b> Código Postal </b> </td>
        <td>01150</td>
  
      </tr>

      <tr>
        <td><b> Télefono </b> </td>
        <td>00 00 00 00</td>
  
      </tr>


    </tbody>
  </table>


</div>


<div class="col-md-6"> 

<h4> Método de Pago </h4>

<p> Paypal , Débito </p>

</div>

</div>


<div class="row">
<div class="col-md-6"> 

<h4> Dirección de Entrega </h4>

<table class="table" id="tabla-shop">
    <thead>

    </thead>
    <tbody>
      <tr>
        <td><b>Nombre</b></td>
        <td>Ernesto</td>
 
      </tr>

      <tr>
        <td> <b> Dirección </b> </td>
        <td>Arte 222</td>
 
      </tr>

      <tr>
        <td> <b> Colonia </b> </td>
        <td>Aragon la Villa</td>
  
      </tr>

      <tr>
        <td> <b> Estado </b> </td>
        <td>D.F</td>
  
      </tr>

      <tr>
        <td> <b> Código Postal </b> </td>
        <td>01150</td>
  
      </tr>

      <tr>
        <td><b> Télefono </b> </td>
        <td>00 00 00 00</td>
  
      </tr>


    </tbody>
  </table>


</div>




<div class="col-md-6"> 

<h4> Rastreo de tu envío </h4>

<p> Número de Guia  <span> 32923 </span> </p>

</div>

</div>



<div class="col-xs-12"> <input id="b_devolucion" type="button" value="Devolución / Cambio"> </div>


<br/> <br/> 

            <h2 class="libreta"> Vendido(s) por Rapsodia </h2>  

            <p> 1 Producto </p> 




<div class="col-md-12"> 


<table class="table" id="tabla-shop">
    <thead>

    </thead>
    <tbody>

      <tr>

        <td><img class="img_final" src="./media/catalog/product/n_1.jpg"></td>
        <td><b> SACO AMUR ABERDEEN</b></td>
        <td><p> 932839F8S92229SF89 </p>  <b> Talla: </b>  22 </td>  
        <td><p> Enviado </p> </td>    
        <td><p> 2.202 MXN </p> </td> 
        <td><b> x1 </b> </td> 
        <td class="align_final"> <p> 3.202 MXN </p> <p> 40% </p> <p> 2.202 MXN </p>  </td> 

      </tr>


    </tbody>
  </table>


<br/> 


<div class="row">
<div class="col-sm-6"></div>

<div class="col-xs-12 col-sm-6"> 
<table class="table price" id="tabla-shop">
    <thead>

    </thead>
    <tbody>

      <tr>
        <td><p>Subtotal</p></td>
        <td><p>2.200 MXN</p></td>
      </tr>

      <tr>
        <td><p>Envío</p></td>
        <td><p>100 MXN</p></td>
      </tr>

      <tr>
        <td><p>Promo</p></td>
        <td><p>0.0 MXN</p></td>
      </tr>


      <tr>
        <td><p>Total</p></td>
        <td><b> 2.300 MXN </b></td>
      </tr>


    </tbody>
  </table>
</div>
</div> <!-- row  --> 



</div>













            </div>
            <!-- direcciones mostrar --> 
         </div>
         <!-- / BIG newsletters -->































      </div>
      <!-- columna 10 --> 
   </div>
</div>
<?php include ('./footer.php'); ?>