<?php include('./header.php'); ?>  

<div class="container-fluid image-locale">  
   <img src="./media/default/banner-locales.jpg" alt="">
</div>


<style>
	
 ul.country li 
 {

 border-right: 1px solid black;  
 padding-right: 8px;  
 padding-left: 8px;  

 }

ul.country { margin-top: 10px;     font-size: .83em; }


 .select-country {  text-decoration: underline; margin-bottom: 5px; }

 .all-shops {  font-size: .83em;  }


</style>



<div class="row top-banner">
	
	<div class="col-xs-3 text-center"><input type="text" placeholder="Región, Ciudad o Tienda"></div>
    <div class="col-xs-6 text-center"> 

    <a href="#" class="select-country">Seleccionar País <img src="http://www.rapsodia.com.mx/skin/frontend/default/rapsodia16ss/images/flecha-select.svg" alt=""></a> 
     

<ul class="country">
	
	<li><a href="#">Argentina</a></li>
    <li><a href="#">Chile</a></li>
    <li><a href="#">Colombia</a></li>
    <li><a href="#">México</a></li>
    <li><a href="#">Paraguay</a></li>
    <li><a href="#">Uruguay</a></li>
    <li><a href="#">Venezuela</a></li>

</ul>


    </div>
    
    <div class="col-xs-3 text-center"> <a href="#" class="all-shops">Ver todas las Tiendas</a></div> 

</div>  





<div class="container"> 
   <div class="row ubica_tienda"> 
      <div class="col-xs-12"> 
         <h3>México</h3> 
      </div> 
   </div>
   <div class="row">
      <div class="filtro filtro-mexico">
         <div class="col-xs-6 col-sm-3">
            <h5><a href="#/" onclick="show_location('1')">Acapulco</a></h5>
         </div>
         <div class="col-xs-6 col-sm-3">
            <h5><a href="#/" onclick="show_location('3')">DF</a></h5>
         </div>
         <div class="col-xs-6 col-sm-3">
            <h5><a href="#/" onclick="show_location('5')">Monterrey</a></h5>
         </div>
         <div class="col-xs-6 col-sm-3">
            <h5><a href="#/" onclick="show_location('7')">Querétaro</a></h5>
         </div>
         <div class="col-xs-6 col-sm-3">
            <h5><a href="#/" onclick="show_location('2')">Cancún</a></h5>
         </div>
         <div class="col-xs-6 col-sm-3">
            <h5><a href="#/" onclick="show_location('4')">Guadalajara</a></h5>
         </div>
         <div class="col-xs-6 col-sm-3">
            <h5><a href="#/" onclick="show_location('6')">Puebla</a></h5>
         </div>
      </div>
   </div>
   <div class="ubica ubicacion-1">
      <div class="row ubica_tienda">
         <div class="col-xs-12">
            <h3>Acapulco</h3>
         </div>
      </div>
      <div class="row">
         <div class="locale-map">
            <div class="col-xs-12 col-sm-3 col-md-3">
               <h5><strong>La Isla</strong></h5>
               <address class="direccion">Blvd. de las Naciones 18, Col. Playa Diamante. Acapulco de Juárez. Municipio de Guerrero.</address>
               <p class="telefono"><span>Tel:</span> <a title="Llamar" href="tel:(52) 7444621933 / 34">(52) 7444621933 / 34</a></p>
            </div>
            <div class="col-xs-8 col-sm-4 col-md-3">
               <table class="table location">
                  <thead>
                     <tr>
                        <th>Horarios</th>
                        <th></th>
                     </tr> 
                  </thead>
                  <tbody>
                     <tr>
                        <td>Lunes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Martes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Miercoles</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Jueves</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Viernes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Sabado</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Domingo</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="col-xs-4 col-sm-5 col-md-6">
               <h5 class="link_tienda">
                  <div class="hidden-xs">
                     <p class="stand" data-text="2" data-lat="16.798818" data-long="-99.815931" onclick="initMap(this)"><span>Ver Mapa</span></p>
                  </div>
                  <a class="hidden-sm hidden-md hidden-lg" href="http://maps.google.com/maps?q=16.798818,-99.815931" target="_blank"><span>Ver Mapa</span></a>
                  <div class="ubicacion" id="m2"></div>
                  
                  <div class="pop_container d2">
                     <h3>La Isla</h3>
                     <p>Blvd. de las Naciones 18, Col. Playa Diamante. </p>
                     <p>Acapulco de Juárez. </p>
                     <p>Municipio de Guerrero.</p>
                  </div>

               </h5>
            </div>
         </div>
      </div>
      <!-- row --> 
   </div>
   <!-- / ubicacion --> 
   <div class="ubica ubicacion-2">
      <div class="row ubica_tienda">
         <div class="col-xs-12">
            <h3>Cancún</h3>
         </div>
      </div>
      <div class="row ">
         <div class="locale-map">
            <div class="col-xs-12 col-sm-3 col-md-3">
               <h5><strong>La Isla</strong></h5>
               <address class="direccion">Blvd. Kukulcan km 12. Benito Juárez. Cancún Quintana Roo.</address>
               <p class="telefono"><span>Tel:</span> <a title="Llamar" href="tel:(52)9981768158 /189">(52)9981768158 /189</a></p>
            </div>
            <div class="col-xs-8 col-sm-4 col-md-3">
               <table class="table location">
                  <thead>
                     <tr>
                        <th>Horarios</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>Lunes</td>
                        <td>10 a 22 hrs</td>
                     </tr>
                     <tr>
                        <td>Martes</td>
                        <td>10 a 22 hrs</td>
                     </tr>
                     <tr>
                        <td>Miercoles</td>
                        <td>10 a 22 hrs</td>
                     </tr>
                     <tr>
                        <td>Jueves</td>
                        <td>10 a 22 hrs</td>
                     </tr>
                     <tr>
                        <td>Viernes</td>
                        <td>10 a 22 hrs</td>
                     </tr>
                     <tr>
                        <td>Sabado</td>
                        <td>10 a 22 hrs</td>
                     </tr>
                     <tr>
                        <td>Domingo</td>
                        <td>10 a 22 hrs</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="col-xs-4 col-sm-5 col-md-6">
               <h5 class="link_tienda">
                  <div class="hidden-xs">
                     <p class="stand" data-text="3" data-lat="21.113269" data-long="-86.759812" onclick="initMap(this)"><span>Ver Mapa</span></p>
                  </div>
                  <a class="hidden-sm hidden-md hidden-lg" href="http://maps.google.com/maps?q=21.113269,-86.759812" target="_blank"><span>Ver Mapa</span></a>
                  <div class="ubicacion" id="m3"></div>
                  <div class="pop_container d3">
                     <h3>La Isla</h3>
                     <p>Blvd. Kukulcan km 12. Benito Juárez.</p>
                     <p>Cancún Quintana Roo. </p>
                     
                  </div>
               </h5>
            </div>
         </div>
      </div>
      <!-- row --> 
   </div>
   <!-- / ubicacion --> 
   <div class="ubica ubicacion-3">
      <div class="row ubica_tienda">
         <div class="col-xs-12">
            <h3>DF</h3>
         </div>
      </div>
      <div class="row ">
         <div class="locale-map">
            <div class="col-xs-12 col-sm-3 col-md-3">
               <h5><strong>Antara Polanco</strong></h5>
               <address class="direccion">Av. Ejercito Nacional 843           Col. Granada,       México D.F.</address>
               <p class="telefono"><span>Tel:</span> <a title="Llamar" href="tel:(5255) 52822736 /2773">(5255) 52822736 /2773</a></p>
            </div>
            <div class="col-xs-8 col-sm-4 col-md-3">
               <table class="table location">
                  <thead>
                     <tr>
                        <th>Horarios</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>Lunes</td>
                        <td>11 a 21:30 hrs</td>
                     </tr>
                     <tr>
                        <td>Martes</td>
                        <td>11 a 21:30 hrs</td>
                     </tr>
                     <tr>
                        <td>Miercoles</td>
                        <td>11 a 21:30 hrs</td>
                     </tr>
                     <tr>
                        <td>Jueves</td>
                        <td>11 a 21:30 hrs</td>
                     </tr>
                     <tr>
                        <td>Viernes</td>
                        <td>11 a 21:30 hrs</td>
                     </tr>
                     <tr>
                        <td>Sabado</td>
                        <td>11 a 21:30 hrs</td>
                     </tr>
                     <tr>
                        <td>Domingo</td>
                        <td>11 a 21:30 hrs</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="col-xs-4 col-sm-5 col-md-6">
               <h5 class="link_tienda">
                  <div class="hidden-xs">
                     <p class="stand" data-text="5" data-lat="19.438841" data-long="-99.204229" onclick="initMap(this)"><span>Ver Mapa</span></p>
                  </div>
                  <a class="hidden-sm hidden-md hidden-lg" href="http://maps.google.com/maps?q=19.438841,-99.204229" target="_blank"><span>Ver Mapa</span></a>
                  <div class="ubicacion" id="m5"></div>
                  <div class="pop_container d5">
                     <h3>Antara Polanco</h3>
                     <p>Av. Ejercito Nacional 843           Col.  </p>
                     <p>Granada,       México D.F.</p>
                    
                  </div>
               </h5>
            </div>
         </div>
      </div>
      <!-- row --> 
      <div class="row ">
         <div class="locale-map">
            <div class="col-xs-12 col-sm-3 col-md-3">
               <h5><strong>Arcos Bosques</strong></h5>
               <address class="direccion">Paseo de los Tamarindos 90. Col.Bosque de las Lomas. México D.F.</address>
               <p class="telefono"><span>Tel:</span> <a title="Llamar" href="tel:(52)21679586 /87">(52)21679586 /87</a></p>
            </div>
            <div class="col-xs-8 col-sm-4 col-md-3">
               <table class="table location">
                  <thead>
                     <tr>
                        <th>Horarios</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>Lunes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Martes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Miercoles</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Jueves</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Viernes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Sabado</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Domingo</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="col-xs-4 col-sm-5 col-md-6">
               <h5 class="link_tienda">
                  <div class="hidden-xs">
                     <p class="stand" data-text="6" data-lat="19.38725" data-long="-99.25224" onclick="initMap(this)"><span>Ver Mapa</span></p>
                  </div>
                  <a class="hidden-sm hidden-md hidden-lg" href="http://maps.google.com/maps?q=19.38725,-99.25224" target="_blank"><span>Ver Mapa</span></a>
                  <div class="ubicacion" id="m6"></div>
                  <div class="pop_container d6">
                     <h3>Arcos Bosques</h3>
                     <p>Paseo de los Tamarindos 90.  </p>
                     <p>Col.Bosque de las Lomas. México D.F</p>
                     
                  </div>
               </h5>
            </div>
         </div>
      </div>
      <!-- row --> 
      <div class="row ">
         <div class="locale-map">
            <div class="col-xs-12 col-sm-3 col-md-3">
               <h5><strong>Condesa</strong></h5>
               <address class="direccion">Av. Tamaulipas 88. Col. Hipódromo Condesa Del. Cuauhtémoc. Ciudad de México, D.F.</address>
               <p class="telefono"><span>Tel:</span> <a title="Llamar" href="tel:(55) 5211 6372, (55) 52116388">(55) 5211 6372, (55) 52116388</a></p>
            </div>
            <div class="col-xs-8 col-sm-4 col-md-3">
               <table class="table location">
                  <thead>
                     <tr>
                        <th>Horarios</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>Lunes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Martes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Miercoles</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Jueves</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Viernes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Sabado</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Domingo</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="col-xs-4 col-sm-5 col-md-6">
               <h5 class="link_tienda">
                  <div class="hidden-xs">
                     <p class="stand" data-text="20" data-lat="19.411017" data-long="-99.174315" onclick="initMap(this)"><span>Ver Mapa</span></p>
                  </div>
                  <a class="hidden-sm hidden-md hidden-lg" href="http://maps.google.com/maps?q=19.411017,-99.174315" target="_blank"><span>Ver Mapa</span></a>
                  <div class="ubicacion" id="m20"></div>
                  <div class="pop_container d20">
                     <h3>Condesa</h3>
                     <p>Av. Tamaulipas 88. Col. Hipódromo  </p>
                     <p>Condesa Del. Cuauhtémoc. Ciudad de </p>
                     <p>México, D.F.</p>
                  </div>
               </h5>
            </div>
         </div>
      </div>
      <!-- row --> 
      <div class="row ">
         <div class="locale-map">
            <div class="col-xs-12 col-sm-3 col-md-3">
               <h5><strong>Paseo Interlomas</strong></h5>
               <address class="direccion">Av. Vialidad de  La Barraca n° 6, Col Ex Hacienda Jesús del Monte. Municipio de Huixquilucan. Estado de México.</address>
               <p class="telefono"><span>Tel:</span> <a title="Llamar" href="tel:(52 555) 290-2768">(52 555) 290-2768</a></p>
            </div>
            <div class="col-xs-8 col-sm-4 col-md-3">
               <table class="table location">
                  <thead>
                     <tr>
                        <th>Horarios</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>Lunes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Martes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Miercoles</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Jueves</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Viernes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Sabado</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Domingo</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="col-xs-4 col-sm-5 col-md-6">
               <h5 class="link_tienda">
                  <div class="hidden-xs">
                     <p class="stand" data-text="26" data-lat="19.396431" data-long="-99.281474" onclick="initMap(this)"><span>Ver Mapa</span></p>
                  </div>
                  <a class="hidden-sm hidden-md hidden-lg" href="http://maps.google.com/maps?q=19.396431,-99.281474" target="_blank"><span>Ver Mapa</span></a>
                  <div class="ubicacion" id="m26"></div>
                  <div class="pop_container d26">
                     <h3>Paseo Interlomas</h3>
                     <p>Av. Vialidad de  La Barraca n° 6, Col Ex  </p>
                     <p>Hacienda Jesús del Monte. Municipio </p>
                     <p>de Huixquilucan. Estado de México.</p>
                  </div>
               </h5>
            </div>
         </div>
      </div>

       <div class="row ">
         <div class="locale-map">
            <div class="col-xs-12 col-sm-3 col-md-3">
               <h5><strong>Premium Outlets Punta Norte </br><img src="./media/default/vintage.svg"></strong></h5>
               <address class="direccion">Hacienda Sierra Vieja N2. Periférico Norte y Autopista Chamapa la Venta Interlomas.</address>
               <p class="telefono"><span>Tel:</span> <a title="Llamar" href="tel:(5255) 2075 0119">(5255) 2075 0119</a></p>
            </div>
            <div class="col-xs-8 col-sm-4 col-md-3">
               <table class="table location">
                  <thead>
                     <tr>
                        <th>Horarios</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>Lunes</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Martes</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Miercoles</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Jueves</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Viernes</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Sabado</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Domingo</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="col-xs-4 col-sm-5 col-md-6">
               <h5 class="link_tienda">
                  <div class="hidden-xs">
                     <p class="stand" data-text="27" data-lat="19.601523" data-long="-99.199856" onclick="initMap(this)"><span>Ver Mapa</span></p>
                  </div>
                  <a class="hidden-sm hidden-md hidden-lg" href="http://maps.google.com/maps?q=19.601523,-99.199856" target="_blank"><span>Ver Mapa</span></a>
                  <div class="ubicacion" id="m27"></div>
                  <div class="pop_container d27">
                     <h3>Premium Outlets Punta Norte</h3>
                     <p>Hacienda Sierra Vieja N2. Periférico  </p>
                     <p>Norte y Autopista Chamapa la Venta </p>
                     <p>Interlomas.</p>
                  </div>
               </h5>
            </div>
         </div>
      </div>
      <!-- row --> 
      <div class="row ">
         <div class="locale-map">
            <div class="col-xs-12 col-sm-3 col-md-3">
               <h5><strong>Reforma 222</strong></h5>
               <address class="direccion">Paseo de la Reforma n° 222. Colonia Juárez. Delegación Cuauhtémoc, México D.F.</address>
               <p class="telefono"><span>Tel:</span> <a title="Llamar" href="tel:5207 6549 / 5207 6193">5207 6549 / 5207 6193</a></p>
            </div>
            <div class="col-xs-8 col-sm-4 col-md-3">
               <table class="table location">
                  <thead>
                     <tr>
                        <th>Horarios</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>Lunes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Martes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Miercoles</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Jueves</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Viernes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Sabado</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Domingo</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="col-xs-4 col-sm-5 col-md-6">
               <h5 class="link_tienda">
                  <div class="hidden-xs">
                     <p class="stand" data-text="36" data-lat="19.429436" data-long="-99.162034" onclick="initMap(this)"><span>Ver Mapa</span></p>
                  </div>
                  <a class="hidden-sm hidden-md hidden-lg" href="http://maps.google.com/maps?q=19.429436,-99.162034" target="_blank"><span>Ver Mapa</span></a>
                  <div class="ubicacion" id="m36"></div>
                  <div class="pop_container d36">
                     <h3>Reforma 222</h3>
                     <p>Paseo de la Reforma n° 222. Colonia   </p>
                     <p>Juárez. Delegación Cuauhtémoc,   </p>
                     <p>México D.F.</p>
                  </div>
               </h5>
            </div>
         </div>
      </div>
      <!-- row --> 

            <!-- row --> 
      <div class="row ">
         <div class="locale-map">
            <div class="col-xs-12 col-sm-3 col-md-3">
               <h5><strong>Santa Fe</strong></h5>
               <address class="direccion">Ampliación Centro Santa Fe. Av. Vasco de Quiroga  3800. Cuajimalpa. México, D.F..</address>
               <p class="telefono"><span>Tel:</span> <a title="Llamar" href="tel:1111-1111">1111-1111</a></p>
            </div>
            <div class="col-xs-8 col-sm-4 col-md-3">
               <table class="table location">
                  <thead>
                     <tr>
                        <th>Horarios</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>Lunes</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Martes</td>
                        <td>11 a 201 hrs</td>
                     </tr>
                     <tr>
                        <td>Miercoles</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Jueves</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Viernes</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Sabado</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Domingo</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="col-xs-4 col-sm-5 col-md-6">
               <h5 class="link_tienda">
                  <div class="hidden-xs">
                     <p class="stand" data-text="50" data-lat="19.362521" data-long="-99.271988" onclick="initMap(this)"><span>Ver Mapa</span></p>
                  </div>
                  <a class="hidden-sm hidden-md hidden-lg" href="http://maps.google.com/maps?q=19.362521,-99.271988" target="_blank"><span>Ver Mapa</span></a>
                  <div class="ubicacion" id="m50"></div>
                  <div class="pop_container d50">
                     <h3>Santa Fe</h3>
                     <p>Ampliación Centro Santa Fe. Av. Vasco </p>
                     <p>de Quiroga  3800. Cuajimalpa. México,  </p>
                     <p>D.F.</p>
                  </div>
               </h5>
            </div>
         </div>
      </div>

   </div>
   <!-- / ubicacion --> 
   <div class="ubica ubicacion-4">
      <div class="row ubica_tienda">
         <div class="col-xs-12">
            <h3>Guadalajara</h3>
         </div>
      </div>
      <div class="row ">
         <div class="locale-map">
            <div class="col-xs-12 col-sm-3 col-md-3">
               <h5><strong>Centro Comercial Andares</strong></h5>
               <address class="direccion">Blvd. Puerta de Hierro 4965. Municipio Zapopan. Guadalajara. Jalisco.</address>
               <p class="telefono"><span>Tel:</span> <a title="Llamar" href="tel:(52) 3336112674">(52) 3336112674</a></p>
            </div>
            <div class="col-xs-8 col-sm-4 col-md-3">
               <table class="table location">
                  <thead>
                     <tr>
                        <th>Horarios</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr> 
                        <td>Lunes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Martes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Miercoles</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Jueves</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Viernes</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Sabado</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                     <tr>
                        <td>Domingo</td>
                        <td>11 a 21 hrs</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="col-xs-4 col-sm-5 col-md-6">
               <h5 class="link_tienda">
                  <div class="hidden-xs">
                     <p class="stand" data-text="52" data-lat="20.720167" data-long="-103.417778" onclick="initMap(this)"><span>Ver Mapa</span></p>
                  </div> 
                  <a class="hidden-sm hidden-md hidden-lg" href="http://maps.google.com/maps?q=20.720167,-103.417778" target="_blank"><span>Ver Mapa</span></a>
                  <div class="ubicacion" id="m52"></div>
                  <div class="pop_container d52">
                     <h3>Centro Comercial Andares</h3>
                     <p>Blvd. Puerta de Hierro 4965. Municipio  </p>
                     <p>Zapopan. Guadalajara. Jalisco. </p>
                     
                  </div>
               </h5>
            </div>
         </div>
      </div>
      <!-- row --> 
   </div>
   <!-- / ubicacion --> 
   <div class="ubica ubicacion-5">
      <div class="row ubica_tienda">
         <div class="col-xs-12">
            <h3>Monterrey</h3>
         </div>
      </div>
      <div class="row ">
         <div class="locale-map">
            <div class="col-xs-12 col-sm-3 col-md-3">
               <h5><strong>Paseo San Pedro</strong></h5>
               <address class="direccion">Av. José Vasconcelos 402. Col. Del Valle. San Pedro Garza Garcia, N.L.</address>
               <p class="telefono"><span>Tel:</span> <a title="Llamar" href="tel: 8183785134 / 8183351924"> 8183785134 / 8183351924</a></p>
            </div>
            <div class="col-xs-8 col-sm-4 col-md-3">
               <table class="table location">
                  <thead>
                     <tr>
                        <th>Horarios</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>Lunes</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Martes</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Miercoles</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Jueves</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Viernes</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Sabado</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Domingo</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="col-xs-4 col-sm-5 col-md-6">
               <h5 class="link_tienda">
                  <div class="hidden-xs"> 
                     <p class="stand" data-text="82" data-lat="25.651426" data-long="-100.359717" onclick="initMap(this)"><span>Ver Mapa</span></p>
                  </div>
                  <a class="hidden-sm hidden-md hidden-lg" href="http://maps.google.com/maps?q=25.651426,-100.359717" target="_blank"><span>Ver Mapa</span></a>
                  <div class="ubicacion" id="m82"></div>
                  <div class="pop_container d82">
                     <h3>Paseo San Pedro</h3>
                     <p>Av. José Vasconcelos 402. Col. Del  </p>
                     <p>Valle. San Pedro Garza Garcia, N.L.</p>

                  </div>
               </h5>
            </div>
         </div>
      </div>
      <!-- row --> 
   </div>
   <!-- / ubicacion --> 
   <div class="ubica ubicacion-6">
      <div class="row ubica_tienda">
         <div class="col-xs-12">
            <h3>Puebla</h3>
         </div>
      </div>
      <div class="row ">
         <div class="locale-map">
            <div class="col-xs-12 col-sm-3 col-md-3">
               <h5><strong>Paseo San Pedro</strong></h5>
               <address class="direccion">Av. José Vasconcelos 402. Col. Del Valle. San Pedro Garza Garcia, N.L.</address>
               <p class="telefono"><span>Tel:</span> <a title="Llamar" href="tel:01 (222) 2250961 / 2252537">01 (222) 2250961 / 2252537</a></p>
            </div>
            <div class="col-xs-8 col-sm-4 col-md-3">
               <table class="table location">
                  <thead>
                     <tr>
                        <th>Horarios</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>Lunes</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Martes</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Miercoles</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Jueves</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Viernes</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Sabado</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Domingo</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="col-xs-4 col-sm-5 col-md-6">
               <h5 class="link_tienda">
                  <div class="hidden-xs"> 
                     <p class="stand" data-text="24" data-lat="19.031139" data-long="-98.231862" onclick="initMap(this)"><span>Ver Mapa</span></p>
                  </div>
                  <a class="hidden-sm hidden-md hidden-lg" href="http://maps.google.com/maps?q=19.031139,-98.231862" target="_blank"><span>Ver Mapa</span></a>
                  <div class="ubicacion" id="m24"></div>
                  <div class="pop_container d24">
                     <h3>Angelópolis</h3>
                     <p>Calle Blvd. del Niño Poblano 2510. Entre  </p>
                     <p>Bvld. Atlixco y Vía Atlixco. Col. Estado  </p>
                     <p>Libre y Soberano de Puebla.</p>
                  </div>
               </h5>
            </div>
         </div>
      </div>
      <!-- row --> 
   </div>
   <!-- / ubicacion --> 
   <div class="ubica ubicacion-7">
      <div class="row ubica_tienda">
         <div class="col-xs-12">
            <h3>Queretaro</h3>
         </div>
      </div>
      <div class="row ">
         <div class="locale-map">
            <div class="col-xs-12 col-sm-3 col-md-3">
               <h5><strong>Antea</strong></h5>
               <address class="direccion">Carretera Querétaro, San Luis Potosí 12401. Ejido El Salitre Delegación Felix Osores Sotomayor Qro. México.</address>
               <p class="telefono"><span>Tel:</span> <a title="Llamar" href="tel:(442) 6881566 / (442) 6881 1562">(442) 6881566 / (442) 6881 1562</a></p>
            </div>
            <div class="col-xs-8 col-sm-4 col-md-3">
               <table class="table location">
                  <thead>
                     <tr>
                        <th>Horarios</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>Lunes</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Martes</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Miercoles</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Jueves</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Viernes</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Sabado</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                     <tr>
                        <td>Domingo</td>
                        <td>11 a 20 hrs</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="col-xs-4 col-sm-5 col-md-6">
               <h5 class="link_tienda">
                  <div class="hidden-xs"> 
                     <p class="stand" data-text="29" data-lat="20.664793" data-long="-100.438139" onclick="initMap(this)"><span>Ver Mapa</span></p>
                  </div>
                  <a class="hidden-sm hidden-md hidden-lg" href="http://maps.google.com/maps?q=20.664793,-100.438139" target="_blank"><span>Ver Mapa</span></a>
                  <div class="ubicacion" id="m29"></div>
                  <div class="pop_container d29">
                     <h3>Antea </h3>
                     <p>Carretera Querétaro, San Luis Potosí  </p>
                     <p>12401. Ejido El Salitre Delegación Felix  </p>
                     <p>Osores Sotomayor Qro. México.</p>
                  </div>
               </h5>
            </div>
         </div>
      </div>
      <!-- row --> 
   </div>
   <!-- / ubicacion --> 
</div>



<?php include ('./footer.php'); ?>