<footer id="invierno">

<div class="container"> 


<div class="row newsletterSuscribe">

<form action="./">  
<div class="col-xs-12 col-md-4"><h3>NEWSLETTER</h3></div>
<div class="col-xs-12 col-md-8">

<input type="text" class="inputNewsletter" placeholder="Ingresa tu mail">
<input class="buttonInvierno" id="" type="submit" value="REGISTRATE">


<div class="nl-error" style="color: indianred; font-size: 0.7em;">¡Favor ingresar un email válido!</div>

<div class="s-success" style="color: forestgreen; font-size: 0.7em;">¡Se ha registrado exitosamente!</div>

</div> 

</form>

</div> 


 
<div class="row"> 
    
<ul id="socialInvierno">
               <li><span class="small hidden-xs">FOLLOW US:</span></li>
               <li id="facebook" class="icono">  <a href="#" title="Seguinos en Facebook" target="_blank">Facebook</a></li>
               <li id="instagram" class="icono"> <a href="#" title="Seguinos en Instagram" target="_blank">Instagram</a></li>
               <li id="pinterest" class="icono"> <a href="#" title="Seguinos en Pinterest" target="_blank">Pinterest</a></li>
               <li id="twitter" class="icono">   <a href="#" title="Seguinos en Twitter" target="_blank">Twitter</a></li>
               <li id="googleplus" class="icono"><a href="#" title="Seguinos en Google+" target="_blank">Google+</a></li>
               <li id="youtube" class="icono">   <a href="#" title="Seguinos en YouTube" target="_blank">YouTube</a></li>
               <li id="vimeo" class="icono">     <a href="#" title="Seguinos en Vimeo" target="_blank">Vimeo</a></li>
               <li id="snapchat" class="icono">  <a href="#" title="Seguinos en Snapchat" target="_blank">Snapchat</a></li>
         
</ul>

</div>



<div class="row">
   
<div class="col-xs-12 col-sm-12 col-md-3">  

<div class="borderTexture"> 
<div class="leftVisualIcon comun_invierno rapsodia"> </div>
<div class="visualText"> <p>RAPSODIA</p>  </div>
</div>

</div>

<div class="col-xs-12 col-sm-12 col-md-3">  

<div class="borderTexture"> 
<div class="leftVisualIcon comun_invierno ayuda"> </div>
<div class="visualText"> <p>AYUDA</p>  </div>
</div>

</div> 

<div class="col-xs-12 col-sm-12 col-md-3">  

<div class="borderTexture"> 
<div class="leftVisualIcon comun_invierno tiendas"> </div>
<div class="visualText"> <p>TIENDAS</p>  </div>
</div>

</div>

<div class="col-xs-12 col-sm-12 col-md-3">   

<div class="borderTexture"> 
<div class="leftVisualIcon comun_invierno newsletter"> </div>
<div class="visualText"> <p>NEWSLETTER</p> </div>
</div>

</div>


</div>




<div class="row informativeTextDivision">
   
<div class="col-xs-4 col-sm-2 divisionInfo"><a href="#">¿Qué es Rapsodia?</a></div>
<div class="col-xs-4 col-sm-2 divisionInfo"><a href="#">Devoluciones</a></div> 
<div class="col-xs-4 col-sm-2 divisionInfo"><a href="#">Cuidado de Prendas</a></div>
<div class="col-xs-4 col-sm-2 divisionInfo"><a href="#">Politicas de Privacidad</a></div>
<div class="col-xs-4 col-sm-2 divisionInfo"><a href="#">Plazos de Entrega</a></div>
<div class="col-xs-4 col-sm-2 divisionInfo"><a href="#">Términos y condiciones</a></div>

</div>




</div>




   <div id="legalesInvierno"> 

      <div class="leftFooterInvierno">
      <a href="http://www.rapsodia.com.co/terminos/">Términos y condiciones</a>
      <a href="http://www.rapsodia.com.co/politica/">Política de Privacidad</a> 
      </div>
      
      <div class="rightFooterInvierno"> 
      <p class="legal">© 2016 <a target="_blank" title="#" href="#">Rapsodia.mx</a> Todos los derechos reservados</p>
      </div>

   </div>


</footer>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="./js/swiper.js"></script>  
<!-- Only Home -->
<script src="./js/home.js"></script>     
<script src="./js/totop.js"></script>   
<!-- /Only Home --> 
<script src="./js/cloudzoom.js"></script> 
<script src="./js/extend_product.js"></script> 
<script src="./js/busqueda.js"></script>  

<script src="./js/locales.js"></script> 

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAPPleXmRbkqgKW7c9lRCQSvTvBo7Ssmyg&signed_in=false"></script> 

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>



<a href="#0" class="cd-top" id="btt">Top</a>



<script>
   


</script>




</body>
</html>