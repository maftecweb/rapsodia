<?php include("./header.php"); ?>
  

<div class="container">
   
<div class="row p_marg">
  <div class="col-md-6 col-sm-6 col-xs-12"> 
  
    <ul class="bread">
     
      <li class="l1">Moda</li>
      <li> | </li> 
      <li class="act">Nombre de Producto</li>
      
    </ul>
  
  </div>
  
  
  <div class="col-md-6 col-sm-6 col-xs-12"> 

    <ul class="bread navf">  
      <!--
      <li><img src="http://www.rapsodia.com.ar/skin/frontend/default/rapsodia16fw/images/flecha-nav-izq.svg"></li>
      <li>  </li>  <li>   </li>
     
      <li><span>ant</span></li>
      <li><span>sig</span></li>
     
      <li><img src="http://www.rapsodia.com.ar/skin/frontend/default/rapsodia16fw/images/flecha-nav-der.svg"></li>
       -->
    </ul>


  </div>
 </div> 


<div class="row">


<div class="col-md-7 col-xs-12"> <!-- COL 7 --> 


<div class="col-xs-9 img_product">

<li class="cloud_icon"></li> 
 
<img class = "cloudzoom" src = "./media/catalog/product/z_1.jpg" data-cloudzoom = "zoomImage: './media/catalog/product/z_1.jpg'" /> 

</div> 


<div class="col-xs-3 thumbnails"> 

<ul class="detail_image">

<li>
<a href="#/" data-img="./media/catalog/product/z_1.jpg" onclick="current_image(this)">
<img class="reset_mg" src="./media/catalog/t_1.jpg"></a>
</li>
 
<li>
<a href="#/" data-img="./media/catalog/product/z_2.jpg" onclick="current_image(this)">
<img class="reset_mg" src="./media/catalog/t_2.jpg"></a>
</li>


</ul>
 
</div>


</div> <!-- / COL 7 --> 


<div class="col-md-4 col-xs-11 prd_details">  


<div class="header_product">
<h1 class="title_product"> Saco Amur Aberdeen </h1>
<p class="price_product"> $ 3022.00 </p>
</div>



<div class="detail_prod">
<h3 class="name_atribute"> Color: <span>Beige</span>  </h3>
<ul class="detail_color"><li>  

 <img src="http://www.rapsodia.com.ar/var/colorselectorplus/swatches/5836.jpg">  

</li>
</ul>
</div>


 

<div class="tallas_div"> 
<h3 class="name_atribute"> Talla: <span></span> </h3>
 
<ul class="detail_talla">   

  <li class="color_container">          <a href="#/" data-size="38" onclick="size_change(this)">38</a> </li>    
  <li class="color_container">          <a href="#/" data-size="40" onclick="size_change(this)">40</a> </li>
  <li class="color_container">          <a href="#/" data-size="42" onclick="size_change(this)">42</a> </li>
  <li class="color_container">          <a href="#/" data-size="44" onclick="size_change(this)">44</a> </li>
  <li class="color_container">          <a href="#/" data-size="46" onclick="size_change(this)">46</a> </li>
  <li class="color_container">          <a href="#/" data-size="48" onclick="size_change(this)">48</a> </li> 

</ul> 

<input id="custom_size" type="hidden" value=""></input>

<a href="#" data-toggle="modal" data-target="#myModal" class="a_guia"> Guía de Tallas </a> 

</div>



<div class="product-options"> 
    
  <div id="cantidad">
    <h3>Cantidad:</h3>

    <select name="qty" id="input-quantity">
      
      <option value="1" selected="">01</option>
      <option value="2">02</option>
      <option value="3">03</option>
      <option value="4">04</option>
      <option value="5">05</option>
      <option value="6">06</option>
      
    </select>

  </div>

  <div id="comprar"> 
    
    <button onclick="location.href='./pc_resumen_carrito.php';" type="button">COMPRAR</button> 
    
  </div>

</div> 



<a href="#" id="link-wishlist"><img src="./media/default/wishlist.svg" alt="Lista de deseos"></a>


 

<div class="row" style="margin-top: 50px;">
   <div class="col-md-12" id="cajadetalles">
      <div id="detalles">
         <h2>Detalles</h2>
         
         <h3 id="gui" class="abierto">Descripción</h3>   
         
         <div class="acd default">
         <p>Chaqueta completamente bordada en canutillos dorados en todo el cuerpo y mangas.  Chaqueta con escote solapada y mangas larga. 
         Forrada a tono. Tejido Premium. Prenda exclusiva</p>
         </div>

         <h3>Material</h3>
          <div class="acd">
         <p>100% Poliéster-Polyester-Poliéster Con Aplicación/Com Aplicação Forro-Lining-Forro: 100% Viscosa-Viscose-Viscose</p>
         </div>

         <h3>Cuidado de la ropa</h3>
         
          <div class="acd">
          <p>Lavado En Seco Con Percloroetileno En Tintorería Tradicional - No Exprimir - No Lavar - No Planchar - No Secar En Máquina - No Usar Blanqueador -</p>
          </div>

         <h3>Medios de Pago</h3> 
         <div class="acd">
            <div><img src="http://www.rapsodia.com.ar/skin/frontend/default/rapsodia16fw/images/mercadopago.png" alt="MercadoPago"></div>
         </div>
         
         <h3>Métodos de Envío</h3>
         
         <div class="acd">
            <p>Podrás elegir entre las siguientes opciones de envío:</p>
            <ul>
               <li>Envío estándar: vía Correos Andreani.</li>
               <li>Retiro en sucursal Andreani.</li>
               <li>Pick up in store – Local Rapsodia: Local DOT o Local Recoleta Mall.</li>
            </ul>
            <p>Para más información consulta <a title="Envío" href="/envio">aquí</a></p>
         </div>

         <h3>Contacto</h3>
         
         <div class="acd"> 
            <div>Atención telefónica: <a href="tel:3221-6869">3221-6869</a></div>
            <div>Escribinos a: <a href="mailto:contacto@rapsodiastore.com.ar">contacto@rapsodiastore.com.ar</a></div>
         </div>

      </div>
   </div>
</div>



</div>


<div class="col-xs-1 scl-mobile">  
 
<div id="producto_social">


<ul>
   
  <li class="social_comun s_facebook"></li>
  <li class="social_comun s_twitter"></li>
  <li class="social_comun s_pinterest"></li>
  <li class="social_comun s_mail"></li>
</ul>


</div>

</div>






</div>


<div class="row relation_prod">

<h2 class="p-arch"><span> YOU MIGHT ALSO LIKE: </span></h2>

 <div class="col-md-5c col-sm-5c"> 
  <img src="./media/catalog/01.jpg"> </div>

 <div class="col-md-5c col-sm-5c"> 
  <img src="./media/catalog/02.jpg"> </div>

 <div class="col-md-5c col-sm-5c"> 
  <img src="./media/catalog/03.jpg"> </div>

  <div class="col-md-5c col-sm-5c"> 
 <img src="./media/catalog/04.jpg"> </div>

  <div class="col-md-5c col-sm-5c"> 
 <img src="./media/catalog/05.jpg"> </div>

</div>


</div>

   


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog tallas" role="document">
    <div class="modal-content" id="modal-talla">
     
       
      <div class="modal-body detail_sz">
        <button type="button" class="close modal-talla" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <img src="http://www.rapsodia.com.ar/skin/frontend/default/rapsodia16fw/images/tabla-talles.jpg?ss16"> 
      </div>
     
    </div>
  </div>
</div>


<?php include("./footer.php"); ?> 
