<?php include('./header.php'); ?>  

<style>

#carga_login { padding-bottom: 9em; }

.espacio_bordes {
	border: 1px solid #bbbbba;
    font-size: .8em;
    padding-right: 10px;
    padding-left: 10px;
    padding-top: 1em;
    margin-bottom: 1.5em;
}


.title-contact { 
 	margin-top: 0;
    border-bottom: 1px solid #1d1d1b;
    color: #1d1d1b;
    text-transform: none;
    font-weight: 400;
    margin-bottom: 1em;
    padding-bottom: .5em;
   
  }

.title-contact, .titulo_login { font-size: 1em; }

.borde_clientes { padding-left: 0; }

.borde_login { padding-right: 0; }

.titulo_login {
    margin: 0;
    color: #000;
    text-transform: uppercase;
    line-height:3em;
  }

 #button_clientes {
	margin-top:12.5em;
	margin-bottom:2em;	
  }

button #button_clientes, button #button_registrado { letter-spacing: 0; } 

#button_registrado {
	margin-bottom:1.8em;
   	margin-top:0;	
  }

 .b_registro {
 	font-size:.8em;
  	margin-right: 4em;
  }

.datos_registrado, .b_registro, .espacio_bordes p, .titulo_login { margin-left: 1em;}

.datos_registrado, .datos_registrado label {
	color:#7e7e7e;
  	font-weight:none;
  	font-style: normal;
  }

textarea:focus, input:focus, .uneditable-input:focus {
  	border-color: rgba(0, 0, 0, 0.8) !important;
  	 box-shadow: 0 1px 1px rgba(0, 0, 0, 0.025) inset, 0 0 8px rgba(0, 0,0, 0.025) !important;
  	 outline: 0 none !important;
}

a.contrasena_olivdada{
  color:#444;
  text-decoration:none;
}


label {font-weight:normal !important;}

@media screen and (max-width: 435px){
.borde_login{
  padding-left: .5em;
  padding-right: .5em;
}
.title-contact{
  margin-left: .5em;
  margin-right: .5em;
}


#button_clientes{
margin-top:.5em;
}

.borde_clientes{
  padding-left: .5em;
  padding-right: .5em;

}

.contrasena_olivdada{
  font-size:.81em; 
}   
}


</style>

<hr>
<div class="container">
   <div class="row">
      <div class="col-md-12 col-sm-12" id="carga_login">
         <h1 class="title-contact">Ingresar o crear una cuenta</h1>
         <div class="col-md-6 col-sm-6 borde_clientes">
            <div class="espacio_bordes">
               <h1 class="titulo_login">NUEVOS CLIENTES</h1>
               <p>Al registrarte en nuestra tienda, se agilizará el proceso de compra, podrás añadir múltiples direcciones de envío, ver y hacer un seguimiento de tus pedidos, y mucho más.</p>
               <button type="submit" class="button b_registro" id="button_clientes">REGISTRATE</button> 
            </div>
         </div>
         <div class="col-md-6 col-sm-6 borde_login" >
            <div class="espacio_bordes" style="/* padding-left: 0; *//* padding-right: 0px; */">
               <h1 class="titulo_login">CLIENTES REGISTRADOS</h1>
               <p>Si tienes una cuenta registrada, ingresa tus datos</p>
               <form role="form">
                  <div class="form-group datos_registrado">
                     <label for="email" class="required">*Dirección de e-mail:</label>
                     <input type="email" class="form-control" id="email">
                  </div>
                  <div class="form-group datos_registrado">
                     <label for="pwd" class="required">*Contraseña:</label>
                     <input type="password" class="form-control" id="pwd">
                  </div>
                  <div class="checkbox obligatorio">
                     <p>*Campos obligatorios</p>
                  </div>
                  <button type="submit" class="button b_registro" id="button_registrado">INGRESAR</button>
                  <a class="contrasena_olivdada" href="#">¿Olvidaste tu contraseña?</a>
               </form>
            </div>
         </div>
      </div>
   </div><!--cierra row-->
</div><!--cierra container-->
<?php include ('./footer.php'); ?>  

