<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> Rapsodia </title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link rel="shortcut icon" href="./media/favicon/favicon.png" type="image/x-icon" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <link rel="shortcut icon" href="./media/favicon/favicon.png" type="image/x-icon" />
  <link rel="stylesheet" type="text/css" href="./css/checkout_style.css" />
</head>

<body>
  <header>
    <div class="container header-carrito">
      <div class="row"><span class="regresar_catalogo"><a href="#">Regresar al catálogo</a></span>
      </div>
      <a href="#"><img src="./media/default/logo-rapsodia.svg" class="logo-checkout">
      </a>
      <li class="elementos-header" id="checkout_envio">
        <div class="titulos-header" href="#" data-toggle="tooltip" data-placement="bottom" title="Horray!">ENVIO GRATIS</div>
        <span class="info-elementos-header">a partir de $899</span>

      </li>
      <li class="elementos-header" id="checkout_cambio">
        <div class="titulos-header">CAMBIO Y DEVOLUCION </div>

        <span class="info-elementos-header">gratis a todo Mèxico</span>
      </li>
      <li class="elementos-header">
        <div class="titulos-header">PAGO EFECTIVO</div>

        <span class="info-elementos-header">en la entrega [+]</span>
      </li>
      <li class="elementos-header" id="checkout_tarjeta">

        <div class="titulos-header">3 MESES SIN</div>
        <span class="info-elementos-header">intereses [+]</span>
      </li>


      <li class="elementos-header" id="checkout_contacto">
        <div class="titulos-header">55 47 50 5020</div>
        <span class="info-elementos-header">contacto</span>
      </li>
      </ul>
    </div>

    </div>

    <header>

      <footer id="footer">
        <div class="box ui-borderTopDotted mvl ptm fss">
          <div class="box izqfloat">
            ¿Tienes preguntas? Déjanos ayudarte.
            <br /> Llama a: 55.4750.5020 </div>
          <div class="box derfloat txt_der">
            <ul class="ui-listaHorizontal">
              <li class="ui-lista_objeto prs ui-borderRightDotted mrs"><a href="/faqs-promoda/" title="Preguntas Frecuentes">Preguntas Frecuentes</a>
              </li>
              <li class="ui-lista_objeto prs ui-borderRightDotted mrs"><a href="/politicas-privacidad-promoda/" title="Política de Privacidad">Política de Privacidad</a>
              </li>
              <li class="ui-lista_objeto"><a href="/terminos-condiciones-promoda/" title="Términos y Condiciones">Términos y Condiciones</a>
              </li>
            </ul>
            <p>&copy; Copyright Promoda</p>
          </div>
        </div>
      </footer>
</body>

</html>