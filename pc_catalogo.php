<?php include ('./header.php'); ?>
 

<div class="container catalogo">  

<div class="row"> 
<div class="col-xs-12 col-sm-6 col-md-6"> 
<div class="row">
 <ol class="breadcrumb new-o">
  <li><a href="#">Inicio</a></li>
  <li><a href="#">Femenino</a></li>
  <li class="">Ropa</li> 
  <li class="">Ropa de Mujer</li>
  <li class="active">Blusas</li>
 </ol>
</div> 
</div>



<div class="col-xs-12 col-sm-6"> 
<div class="row">
<div class="form-group">
 
  <select class="form-control" id="popularidad_catalogo">
    <option>Popularidad</option>
    <option>2</option>
    <option>3</option>
    <option>4</option>

  </select>


<ul class="re">

<li>1</li> 
<li>2</li>
<li>3</li>
<li><i class="fa fa-caret-right" aria-hidden="true"></i></li>
<li><i class="fa fa-fast-forward" aria-hidden="true"></i></li>

</ul>
</div>
</div>


</div>

</div>

<hr style="margin-top: 0px;">    

<div class="row">  
 
 
 <a href="#/" class="btn btn-lg mobile_filtros" data-toggle="modal" data-target="#filters_modal"> Filtros </a> 


<!-- Modal -->
<div class="modal fade" id="filters_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    
    <div class="modal-content"> 
     
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="title_filter">Productos Encontrados</h4>
      </div>

      <div class="modal-body controles">
    
<select class="form-control">
  <option>TALLA</option>
  <option>2</option>
  <option>3</option>
  <option>4</option>
  <option>5</option>
</select>



<select class="form-control">
  <option>DESCUENTO</option>
  <option>2</option>
  <option>3</option>
  <option>4</option>
  <option>5</option>
</select>


<div class="center_ok">
<button type="button">OK</button>
</div>

      </div>
    
    </div>
  
  </div>
</div>






<div class="col-xs-12 col-sm-3 hidden-xs"> <!-- Filtros DESK --> 

 <p class="title-p" id="talla-ropa"> <strong>- </strong> TALLA (ROPA TOP) </p>   

       <div class="scrollbar" id="caja-check">  
          
          <div class="checkbox"> 
              
              <ul class="price-list">
              <li><label><input type="checkbox">2</label></li>
              <li><label><input type="checkbox">4</label></li>
              <li><label><input type="checkbox">6</label></li>
              <li><label><input type="checkbox">8</label></li>
              <li><label><input type="checkbox">10</label></li>
              <li><label><input type="checkbox">12</label></li>
              <li><label><input type="checkbox">14</label></li>
              <li><label><input type="checkbox">32</label></li>
              <li><label><input type="checkbox">34</label></li>
              <li><label><input type="checkbox">36</label></li>
              <li><label><input type="checkbox">38</label></li>
              <li><label><input type="checkbox">40</label></li>
              <li><label><input type="checkbox">42</label></li>
              <li><label><input type="checkbox">44</label></li>
              </ul>

          </div> 
<hr>
       </div><!--cerramos scrollbar -->



  <p class="title-p" id="title-descuento"><strong>-</strong>DESCUENTO</p>

<div id="t_slider">

<div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
   <div class="ui-sliderLine"></div>
   <div class="ui-slider-range ui-widget-header" style="left: 0%; width: 100%;"></div>
   <a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a>
   <a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 100%;"></a>


</div>

<p id="amount" class="slider_amount"><span class="float-left">230MXN </span><span class="float-right">3112MXN </span></p>
       
</div>

      
         <hr> 


</div> <!-- FILTROS DESK -->



<style>
  
.product-lst a:hover { text-decoration: none; color: #000;   }

</style>
 


<div class="col-xs-12 col-sm-9"> 

<div class="row product-lst">

<div class="col-xs-12 col-sm-6 col-md-4"> 
<a href="./pc_detalle_producto.php"><img src="./media/catalog/01.jpg" onmouseover="this.src='media/catalog/04.jpg';" onmouseout="this.src='./media/catalog/01.jpg';"/></a>
<a href="./pc_detalle_producto.php"><h5 class="in-tit"> Falda Peony </h5> </a> 
<span class="itm-flag">NUEVO</span> 
<strike>$&nbsp;2,900</strike>
<p class="in-p">$&nbsp;2,400</p> 

</div>
 
<div class="col-xs-12 col-sm-6 col-md-4"> 
<img src="./media/catalog/02.jpg" onmouseover="this.src='media/catalog/04.jpg';" onmouseout="this.src='./media/catalog/02.jpg';"/>
<h5 class="in-tit"> Falda Ophelia Sari </h5>
<span class="itm-flag">40%</span>
<strike>$&nbsp;2,500</strike>
<p class="in-p">$&nbsp;2,000</p>
</div>
 
<div class="col-xs-12 col-sm-6 col-md-4"> 
<img src="./media/catalog/03.jpg" onmouseover="this.src='media/catalog/04.jpg';" onmouseout="this.src='./media/catalog/03.jpg';"/>
<h5 class="in-tit"> Falda Jinxi </h5>
<span class="itm-flag">NUEVO</span>
<strike>$&nbsp;1,900</strike>
<p class="in-p">$&nbsp;1,400</p>
</div>

<div class="col-xs-12 col-sm-6 col-md-4"> 
<img src="./media/catalog/04.jpg" onmouseover="this.src='media/catalog/03.jpg';" onmouseout="this.src='./media/catalog/04.jpg';"/>
<h5 class="in-tit"> Falda Ophelia Solid </h5>
<span class="itm-flag">15%</span>
<strike>$&nbsp;2,200</strike>
<p class="in-p">$&nbsp;2,000</p>
</div>


<div class="col-xs-12 col-sm-6 col-md-4"> 
<img src="./media/catalog/05.jpg" onmouseover="this.src='media/catalog/04.jpg';" onmouseout="this.src='./media/catalog/05.jpg';"/>
<h5 class="in-tit"> Falda Nyx </h5>
<span class="itm-flag">NUEVO</span>
<strike>$&nbsp;2,900</strike>
<p class="in-p">$&nbsp;2,200</p>
</div>

<div class="col-xs-12 col-sm-6 col-md-4"> 
<img src="./media/catalog/06.jpg" onmouseover="this.src='media/catalog/04.jpg';" onmouseout="this.src='./media/catalog/06.jpg';"/>
<h5 class="in-tit"> Falda Ophelia Corta </h5>
<span class="itm-flag">20%</span>
<strike>$&nbsp;2,900</strike>
<p class="in-p">$&nbsp;1,200</p>
</div>




</div>


</div>

</div>


</div>



<?php include ('./footer.php'); ?>

