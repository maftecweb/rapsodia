<?php include('./header.php'); ?>   
<hr class="div_fq">
<div class="container">
   <div class="row" id="fq_sections"> 
      <h1> Plazos de Entrega </h1>
      <ul class="f_child">
         <li>
            <p> Los productos comprados en www.rapsodia.com.mx se entregarán en lapso máximo de 5 días naturales posterior a la compra.</p>
         </li>
         <li>
            <p> Rapsodia se compromete a entregar el producto en perfecto estado, en la dirección que el cliente señale en el formulario de pedido, siempre que éste se ubique dentro de la República Mexicana, en el plazo mencionado en el punto anterior.
            </p>
         </li>
         <li>
            <p>Cada entrega se considera efectuada a partir del momento en el cual la empresa de transportes pone el producto a disposición del cliente en el domicilio señalado por éste al momento de realizar la compra, que se materializa a través del sistema de control utilizado por la compañía de transportes contratada, vía la guía de transporte.</p>
         </li>
      </ul>
      <h2 style="text-transform: none;">Si pasados 7 (siete) días hábiles, tras la salida a reparto del pedido, la entrega del mismo no se ha podido concertar, el cliente deberá ponerse en contacto con Rapsodia. En caso de que el cliente no proceda así, pasados 8 (ocho) días hábiles desde la salida a reparto del pedido, éste será devuelto a los almacenes de Rapsodia.</h2>
      <h2 style="text-transform: none;"> Si el motivo por el que no se ha podido realizar la entrega es el extravío del paquete, nuestro transportista iniciará una investigación con tiempo de respuesta de 21 días. </h2>
      <ul class="f_child">
         <li>
            <p> Los productos comprados en www.rapsodia.com.mx se entregarán en lapso máximo de 5 días naturales posterior a la compra.</p>
         </li>
         <li>
            <p> Rapsodia se compromete a entregar el producto en perfecto estado, en la dirección que el cliente señale en el formulario de pedido, siempre que éste se ubique dentro de la República Mexicana, en el plazo mencionado en el punto anterior.
            </p>
         </li>
      </ul>
   </div>
</div>
<?php include ('./footer.php'); ?>