<?php include('./header.php'); ?>
<hr/>
<div class="container" id="cart_detail">
   <h3>Mis Compras</h3>
   <div class="row" id="cart_margin">
      <div class="col-md-8" id="resume_product">
         <table class="table table-condensed">
            <thead>
               <tr>
                  <th>Producto</th>
                  <th>Descripcion</th>
                  <th class="hidden-xs">Precio Unitario</th>
                  <th class="t c">Cantidad</th>
                  <th class="t c">Subtotal</th>
                  <th class="t c"><span class="hidden-xs">Quitar</span></th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td>
                     <img class="i_d" src="./media/catalog/product/n_1.jpg"/>
                  </td>
                  <td class="mg">
                     <p class="d_name">Saco Amur Aberdeen</p>
                     <p class="d_color">Color: <span> <img class="swatch" src="./media/catalog/c_1.jpg" alt="Palido"> </span></p>
                     <p class="d_size">Talla: <span> 40 </span></p>
                  </td>
                  <td class="mg c hidden-xs">
                     <p>$1.300 </p>
                  </td>
                  <td class="mg c">  
                     <input   name="cart" value="1" size="4" title="Cantidad" class="input-text qty plus_get1" maxlength="12">
                     <button  type="button" a_ref="-" data-ref="plus_get1" class="cant_car"  title="Menos">-</button>
                     <button  type="button" a_ref="+" data-ref="plus_get1" class="cant_car"  title="Más">+</button>
                  </td>
                  <td class="mg c">
                     <p>$&nbsp;1.300</p>
                     <span class="delete_prd visible-xs"><a href="#"> X </a></span> 
                  </td>
                  <td class="mg c hidden-xs">x</td>
               </tr>
               <tr>
                  <td>
                     <img class="i_d" src="./media/catalog/product/n_2.jpg"/>
                  </td>
                  <td class="mg">
                     <p class="d_name">Saco Amur Aberdeen</p>
                     <p class="d_color">Color: <span> <img class="swatch" src="./media/catalog/c_1.jpg" alt="Palido"> </span></p>
                     <p class="d_size">Talla: <span> 40 </span></p>
                  </td>
                  <td class="mg c hidden-xs">
                     <p>$1.300 </p>
                  </td>
                  <td class="mg c"> 
                     <input name="cart" value="1" size="4" title="Cantidad" class="input-text qty plus_get2" maxlength="12">
                     <button  type="button" a_ref="-" data-ref="plus_get2" class="cant_car" title="Menos">-</button>
                     <button  type="button" a_ref="+" data-ref="plus_get2"  class="cant_car" title="Más">+</button>
                  </td>
                  <td class="mg c">
                     <p>$&nbsp;1.300</p>
                     <span class="delete_prd visible-xs"><a href="#"> X </a></span> 
                  </td>
                  <td class="mg c hidden-xs">x</td>
               </tr>
            </tbody>
         </table>
      </div>
      <div class="col-md-4" id="resume_shop">
         <h3>Resumen de Compra</h3>
         <h3 class="subtotal">Subtotal <span> $2.100 </span></h3>
         <h3 class="subtotal cupon">Cupón <span> - $100 </span></h3>
         <h3 class="total_general">TOTAL GENERAL <span> $2.000 </span></h3>
         <button type="button" title="Completar Compra" 
            class="button btn-proceed-checkout btn-checkout final"> <span>COMPLETAR COMPRA</span></button>
         <p class="continuar_pedido"><a href="#"> < CONTINUAR COMPRANDO </a></p>
      </div>
   </div>
   <!-- /row --> 
</div>
<!-- /container -->
<?php include ('./footer.php'); ?>