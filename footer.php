<footer> 
   <div id="f_deco">
      <div class="container">
         <div class="col-xs-12 col-sm-6 col-md-3">
            <ul>
               <li class="comun_footer rapsodia"></li>
               <li>
                  <h3>Rapsodia</h3> 
               </li>
               <li><a href="#">¿Qué es Rapsodia?</a></li>
               <li><a href="#">Tiendas </a></li>
               <li><a href="#">Proveedores </a></li>
               <li><a href="#">Facturación</a></li>
            </ul>
         </div>
         <div class="col-xs-12 col-sm-6 col-md-3">
            <ul>
               <li class="comun_footer ayuda"></li>
               <li>
                  <h3>Ayuda</h3>
               </li>
               <li><a href="./preguntas_frecuentes.php">Preguntas Frecuentes</a></li>
               <li><a href="./devoluciones.php">Devoluciones</a></li>
               <li><a href="./politicas_de_privacidad.php">Políticas de Privacidad</a></li>
               <li><a href="./plazos_de_entrega.php">Plazos de Entrega</a></li>
               <li><a href="./terminos_y_condiciones.php">Términos y condiciones</a></li>
            </ul>
         </div> 
         <div class="col-xs-12 col-sm-6 col-md-3">
            <ul>
               <li class="comun_footer tiendas"></li> 
               <li>
                  <h3>Buscador de Tiendas</h3>
               </li>
               <li><a href="#">Ver Tiendas</a></li>
            </ul>
         </div>
         <div class="col-xs-12 col-sm-6 col-md-3">
            <ul>
               <li class="comun_footer newsletter"></li>
               <li>
                  <h3>Newsletter</h3>
               </li>
               <div class="input-box">
                  <input type="text" name="email" id="newsletterb" title="Apuntarse a nuestro boletín de noticias" placeholder="E-mail" class="input-text required-entry validate-email"> 
               </div>
               <div class="actions">
                  <button type="submit" title="Suscríbete" class="button newsletter">Suscríbete</button>
               </div>
            </ul>
         </div>
         <div class="col-xs-12 col-sm-12 col-md-12">
            <ul id="social">
               <li><span class="small hidden-xs">Follow us:</span></li>
               <li id="facebook" class="icono">  <a href="#" title="Seguinos en Facebook"  target="_blank">Facebook</a></li>
               <li id="instagram" class="icono"> <a href="#" title="Seguinos en Instagram" target="_blank">Instagram</a></li>
               <li id="pinterest" class="icono"> <a href="#" title="Seguinos en Pinterest" target="_blank">Pinterest</a></li>
               <li id="twitter" class="icono">   <a href="#" title="Seguinos en Twitter"   target="_blank">Twitter</a></li>
               <li id="googleplus" class="icono"><a href="#" title="Seguinos en Google+"   target="_blank">Google+</a></li>
               <li id="youtube" class="icono">   <a href="#" title="Seguinos en YouTube"   target="_blank">YouTube</a></li>
               <li id="vimeo" class="icono">     <a href="#" title="Seguinos en Vimeo"     target="_blank">Vimeo</a></li>
               <li id="snapchat" class="icono">  <a href="#" title="Seguinos en Snapchat"  target="_blank">Snapchat</a></li>
               <li class="bannerRedes">          
                  <a href="#" target="_blank"><img class="img-responsive" alt="#" src="./media/relevant/f_1.jpg"></a> 
               </li>
            </ul>
         </div>
      </div>
   </div>
   <div id="legales">
      <a href="http://www.rapsodia.com.co/terminos/">Términos y condiciones</a>
      <a href="http://www.rapsodia.com.co/politica/">Política de Privacidad</a>
      <p class="legal">© 2016 <a target="_blank" title="#" href="#">Rapsodia.mx</a> Todos los derechos reservados</p>
      </p>
   </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="./js/swiper.js"></script>  
<!-- Only Home -->
<script src="./js/home.js"></script>     
<script src="./js/totop.js"></script>   
<!-- /Only Home --> 
<script src="./js/cloudzoom.js"></script> 
<script src="./js/extend_product.js"></script> 
<script src="./js/busqueda.js"></script>  

<script src="./js/locales.js"></script> 

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAPPleXmRbkqgKW7c9lRCQSvTvBo7Ssmyg&signed_in=false"></script> 

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>



<a href="#0" class="cd-top" id="btt">Top</a>



<script>
   


</script>




</body>
</html>