<!DOCTYPE html>  
<html lang="es">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->   
      <title> Rapsodia </title>
      <!-- Latest compiled and minified CSS -->  
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
      <link rel="stylesheet" href="./css/slide/swiper.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="./css/styles.css" />
      <link rel="stylesheet" type="text/css" href="./css/footer-invierno.css" /> 

      <link rel="stylesheet" type="text/css" href="./css/locales.css" />

      <link rel="stylesheet" type="text/css" href="./css/new_home.css" />

      <link rel="stylesheet" type="text/css" href="./css/cloudzoom.css" />
      <link rel="shortcut icon" href="./media/favicon/favicon.png" type="image/x-icon" />
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <a name="top"></a> 
      <!-- Nav Details -->
      <nav class="navbar navbar-static-top">
         <div class="container">
            <div id="cls">
               <ul class="nav navbar-nav navbar-left">
                  <li><a href="#about">TIENDAS</a></li>
                  <li class="hidden-xs l-esp"> | </li>
                  <li><a href="#">ATENCIÓN AL CLIENTE</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li><a href="./login.html">ENTRAR </a></li>
                  <li class="hidden-xs l-esp"> | </li>
                  <li><a href="./registro.html">REGÍSTRATE </a></li>
               </ul>
            </div>
         </div>
      </nav>
      <!-- /Nav Details -->
      <nav class="navbar navbar-static-top">
         <div class="container"> 
            <!-- MB -->
            <div class="navbar-header center">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
               <span class="sr-only">MB</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span> 
               </button>
               <div class="h_logo"><a href="./index.php"><img class="logo" src="./media/default/logo-rapsodia.svg"></a></div>
               <div class="rapsodia_shop"> <a id="numero-carrito" href="#" title="Mis Compras"><span> 8 </span></a>  </div>
            </div>
            <!-- /MB -->  
            <div id="navbar" class="navbar-collapse collapse">
               <ul class="nav navbar-nav ref">
                  <li><a href="#">INVIERNO 16</a></li> 
                  <li><a href="#">NEW IN!</a></li>
                  <li class="dropdown dropdown-large">
                     <a id="drop-to" href="#" class="dropdown-toggle" data-toggle="dropdown"> MODA </a>   
                     <ul class="dropdown-menu dropdown-menu-large row change-f">
                        <li class="col-sm-4">
                           <ul>
                              <li class="dropdown-header title">MODA</li>
                              <li><a href="#">Sudaderas y Sweaters</a></li>
                              <li><a href="#">Camisas y Tops</a></li>
                              <li><a href="./pc_catalogo.php">Faldas</a></li>   
                              <li><a href="#">Vestidos y Tunicas</a></li>
                              <hr/>
                           </ul>
                        </li>
                        <li class="col-sm-4">
                           <ul>
                              <li class="dropdown-header title">TENDENCIAS</li>
                              <li><a href="#">Cinturones</a></li>
                              <li><a href="#">Calzado</a></li>
                              <hr/>
                           </ul>
                        </li>
                        <li class="hidden-xs col-sm-4 m-l"> 
                           <img src="./media/catalog/product/h_1.jpg"/>
                        </li>
                     </ul>
                  </li>
                  <li><a href="#">ACCESORIOS</a></li>
                  <li><a href="#">WE LOVE</a></li>
                  <li>  
                     <a href="#" style="padding-top: 6px;">
                     <img class="vint" src="./media/default/vintage.svg"/> 
                     </a>
                  </li>
               </ul>




<div class="search_cart"> 
  
  <div id="buscador">

  <div class="hidden-xs dk-cart">  

  <a href="#"> <img src="./media/default/minicart.png"> (2) </a> 

  </div> 

   <form id="mini_search" action="#" method="get" style="display: inline-block;">
    
     <div class="form-search"> 
        
        <input id="search" placeholder="Buscar" type="text" name="q" class="input-text" maxlength="128" autocomplete="off" autofocus>

        <button class="btn-search" onmouseover="show_search()" type="submit" title="Buscar"><img src="./media/default/search.png"/></button> 
 
     </div>

  </form>

  </div> 
  
</div>
            


            </div>
         </div>
      </nav>


