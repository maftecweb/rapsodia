<?php include('./header.php'); ?>  
<hr>
<div class="container">
   <div class="row" id="fq_sections">
      <h1>Devoluciones</h1>
      <h2>Condiciones generales:</h2>
      <ol class="fq_points">
         <li>
            <p>
               RAPSODIA &nbsp;se hace cargo del costo de la entrega del cambio o la devolución de la compra;
            </p>
         </li>
         <li>
            <p>
               Puedes cambiar tu compra por cualquier otro producto del sitio o en los locales, de la misma colección&nbsp;ó puedes escoger por obtener una eCard por el importe de tu compra.
            </p>
         </li>
         <li>
            <p>
               La solicitud de cambio debe ser hecha hasta 30 (treinta) días corridos, contados a partir de la fecha de recepción del pedido. Las solicitudes recibidas después de este plazo no serán aceptadas;
            </p>
         </li>
         <li>
            <p>
               La eCard podrá ser utilizada solamente en nuestro estore. Se enviará el código a tu correo electrónico, que deberás aplicar al momento de la nueva compra cuyo envío será bonificado.
            </p>
            <p>
               Las eCard tienen un solo uso y validez por 6 meses. El importe del código debe ser utilizado en su totalidad, no siendo acumulable para sucesivas compras.
            </p>
            <p>
               Si tienes alguna duda, contáctanos a través de nuestra Central de Atención al Cliente, a través del mail
               <a href="mailto:contacto@rapsodiastore.com.ar">contacto@rapsodiastore.com.ar</a>
            </p>
         </li>
         <li>
            <p>
               Productos personalizados y con ajustes (dobladillo, pinzas, etc.) no podrán ser cambiados o devueltos.
            </p>
         </li>
         <li>
            <p>
               Las prendas Vintage podrán ser cambiadas únicamente en nuestros locales Vintage o por Correo.
            </p>
         </li>
      </ol>
      <h2>Cambios:</h2>
      <p>
         La política de cambio y devolución de Rapsodia es parecida a la de sus tiendas físicas. Es decir, las prendas sólo serán aceptadas si son devueltas en las siguientes condiciones:
      </p>
      <p>Con etiqueta y lacre del fabricante intactas;</p>
      <p>
         En el packaging original, no damnificado. Cajas/Fundas protectoras de zapato, bolsitas de bijou, bolsitas de plástico y adhesivos de protección de mallas y bikinis, garantías y folletos explicativos son considerados parte del producto;
      </p>
      <p>
         No lavadas ni usadas, sin olores ni manchas, sin marcas en la suela ni alteraciones hechas por el cliente (ej: dobladillo, pinzas, etc.);
      </p>
      <p>Presentar comprobante de compra.</p>
      <h2>Recordá!</h2>
      <p>
         En el caso de que RAPSODIA entienda que el producto devuelto no se encuadra en los criterios arriba descriptos, el sitio tiene derecho a no aceptar la devolución o realizar el cambio. Rapsodia puede, entonces, reenviar el producto al cliente, sin consulta previa, acompañada del &nbsp;justificativo y motivo del rechazo.
      </p>
   </div>
</div>
<?php include ('./footer.php'); ?>