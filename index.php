<?php include ('./header.php'); ?> 


<div class="first container">


   <div class="second swiper-container">

      <div class="swiper-wrapper">
         <div class="swiper-slide"><img class="img-responsive" src="./media/banners/o_1.jpg"></div>
         <div class="swiper-slide"><img class="img-responsive" src="./media/banners/o_1.jpg"> </div>
         <div class="swiper-slide"><img class="img-responsive" src="./media/banners/o_1.jpg"> </div>
      </div>
 
      <!-- Add Pagination  <div class="swiper-pagination"></div>-->

      <!-- Add Arrows -->
      <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div>

   </div>



<div class="row">	

<div class="col-xs-12">	<h2 class="p-arch"><span>SHOP OUR STYLES</span></h2> </div>

</div>



   <div class="row bnrs">

      <div class="col-xs-12 col-sm-3">  

         <img class="img-responsive" src="./media/relevant/hm1.jpg"> 
         <div class="m_texto">
         <h3>Legends of Fall</h3>                  
         <p>Sacos y Camperas</p>
         </div>


      </div>
      <div class="col-xs-12 col-sm-3">  

         <img class="img-responsive" src="./media/relevant/hm2.jpg">   
         <div class="m_texto">
         <h3>GO LONG</h3>                  
         <p>Pantalones</p>
         </div>

      </div>
      <div class="col-xs-12 col-sm-3"> 

         <img class="img-responsive" src="./media/relevant/hm3.jpg"> 
         <div class="m_texto">
         <h3>DRESS TO LOVE</h3>                  
         <p>Vestidos</p>
         </div>

      </div>
      <div class="col-xs-12 col-sm-3"> 

         <img class="img-responsive" src="./media/relevant/hm4.jpg"> 
         <div class="m_texto">
         <h3>FROM THE TOP</h3>                  
         <p>Camisas y Tops</p>
         </div>

      </div>
   </div>



<div class="row send_pk"> 	

<div class="col-xs-12">	
<img class="img-responsive" src="./media/banners/g1.gif" alt=""> 
</div>

</div> 





<div class="row"> 

<div class="col-xs-12"> <h2 class="p-arch"><span>FW 16</span></h2> </div>

</div>



<div class="row two_sides"> 

<div class="col-xs-6"> 
<h3>Fall Collection</h3>
<img class="img-responsive" src="./media/banners/6_1.jpg" alt=""> 

</div>


<div class="col-xs-6 brd_left"> 
<h3>The Lookbook</h3>
<img class="img-responsive" src="./media/banners/6_2.jpg" alt=""> 

</div>

</div>











   <div class="row alt">
      <div class="col-xs-12">
         <div id="sliderRecomendados">
            <h2 class="p-arch"><span>WE LOVE</span></h2>
            <div class="titulowelove">
               <div>
                  <h3>OUR PICKS</h3>
                  <p>Nuestra mirada
                     <br> y los Must Have
                     <br> de esta temporada.
                  </p>
               </div>
            </div>
            <div class="wrapper multi-slide"> 
               <ul class="carousel is-set">
                  <li class="carousel-seat itemGaleria configurable"  style="order: 1;">
                     <a class="product-image" href="#">
                        <img  class="img-responsive imagenproducto" src="./media/catalog/product/h_1.jpg" alt="#" width="300" height="450">
                        <div class="datos">
                           <h3 class="nombre">Sweater Marilen</h3>
                           <div class="price-box">
                              <span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="regular-price" id="product-price-279204">
                              <span itemprop="price">
                              <span class="price">$&nbsp;3.800</span> 
                              </span>
                              </span>
                           </div>
                        </div> 
                     </a>
                  </li>
                  <li class="carousel-seat itemGaleria configurable"  style="order: 2;">
                     <a class="product-image" href="#">
                        <img  class="img-responsive imagenproducto" src="./media/catalog/product/h_2.jpg" alt="#" width="300" height="450">
                        <div class="datos">
                           <h3 class="nombre">Sweater Ogorman</h3>
                           <div class="price-box">
                              <span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="regular-price" id="product-price-279214">
                              <span itemprop="price">
                              <span class="price">$&nbsp;2.700</span> </span>
                              </span>
                           </div>
                        </div>
                     </a>
                  </li>
                  <li class="carousel-seat itemGaleria configurable"  style="order: 3;">
                     <a class="product-image" href="#">
                        <img  class="img-responsive imagenproducto" src="./media/catalog/product/h_3.jpg" alt="#" width="300" height="450">
                        <div class="datos">
                           <h3 class="nombre">Pantalón Davao Onix</h3>
                           <div class="price-box">
                              <span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="regular-price" id="product-price-279399">
                              <span itemprop="price">
                              <span class="price">$&nbsp;2.700</span> </span>
                              </span>
                           </div>
                        </div>
                     </a>
                  </li>
                  <li class="carousel-seat itemGaleria configurable"  style="order: 4;">
                     <a class="product-image" href="#">
                        <img  class="img-responsive imagenproducto" src="./media/catalog/product/h_4.jpg" alt="#" width="300" height="450">
                        <div class="datos">
                           <h3 class="nombre">Camisa Arcos</h3>
                           <div class="price-box"> 
                              <span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="regular-price" id="product-price-279289">
                              <span itemprop="price">
                              <span class="price">$&nbsp;1.500</span> 
                              </span>
                              </span>
                           </div>
                        </div>
                     </a>
                  </li>
                  <li class="carousel-seat itemGaleria configurable"  style="order: 5;">
                     <a class="product-image" href="#">
                        <img  class="img-responsive imagenproducto" src="./media/catalog/product/h_5.jpg" alt="#" width="300" height="450">
                        <div class="datos">
                           <h3 class="nombre">Chaleco Atkins Harbor</h3>
                           <div class="price-box"> 
                              <span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="regular-price" id="product-price-279279">
                              <span itemprop="price">
                              <span class="price">$&nbsp;3.700</span> 
                              </span>
                              </span>
                           </div>
                        </div>
                     </a>
                  </li> 
                  <li class="carousel-seat itemGaleria configurable"  style="order: 6;">
                     <a class="product-image" href="#">
                        <img  class="img-responsive imagenproducto" src="./media/catalog/product/h_6.jpg" alt="#" width="300" height="450">
                        <div class="datos">
                           <h3 class="nombre">Cinturón Floyd</h3>
                           <div class="price-box">
                              <span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="regular-price" id="product-price-287196">
                              <span itemprop="price">
                              <span class="price">$&nbsp;890</span> </span>
                              </span>
                           </div>
                        </div>
                     </a>
                  </li>
                  <li class="carousel-seat itemGaleria configurable"  style="order: 7;">
                     <a class="product-image" href="#">
                        <img  class="img-responsive imagenproducto" src="./media/catalog/product/h_7.jpg" alt="#" width="300" height="450">
                        <div class="datos">
                           <h3 class="nombre">Saco Arion Lynxi</h3>
                           <div class="price-box">
                              <span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="regular-price" id="product-price-279228">
                              <span itemprop="price">
                              <span class="price">$&nbsp;5.600</span> </span>
                              </span>
                           </div>
                        </div>
                     </a>
                  </li>
  
               </ul>
            </div>
            <div class="controls"> 
               <button class="flecha prev toggle" onclick="re_valor('l')"></button>
               <button class="flecha next toggle" onclick="re_valor('r')"></button>
            </div>
         </div>
      </div>
   </div>





<div class="row"> 

<div class="col-xs-12">  
<img src="./media/banners/x_1.jpg" alt="">
</div>

</div>







<div class="row"> 
<div class="col-xs-12 h_social">  

         <h2 class="p-arch"><span>Shop our Pinterest</span></h2>
         <a href="https://www.instagram.com/rapsodiaoficial/"><p class="tar">@rapsodiaoficial</p></a>

</div>
</div>


<div class="row insta">
         <div class="col-xs-4 col-sm-2">  <img class="img-responsive" src="./media/banners/p1.jpg"> </div>
         <div class="col-xs-4 col-sm-2">  <img class="img-responsive" src="./media/banners/p2.jpg"> </div>
         <div class="col-xs-4 col-sm-2">  <img class="img-responsive" src="./media/banners/p3.jpg"> </div>
         <div class="col-xs-4 col-sm-2">  <img class="img-responsive" src="./media/banners/p4.jpg"> </div>
         <div class="col-xs-4 col-sm-2">  <img class="img-responsive" src="./media/banners/p5.jpg"> </div>
         <div class="col-xs-4 col-sm-2">  <img class="img-responsive" src="./media/banners/p6.jpg"> </div>
</div>





<div class="row"> 
<div class="col-xs-12 h_social">  

         <h2 class="p-arch"><span>Follow us on Instagram</span></h2>
         <a href="https://www.instagram.com/rapsodiaoficial/"><p class="tar">@rapsodiaoficial</p></a>

</div>
</div>


<div class="row insta">
         <div class="col-xs-12 col-sm-4">  <img class="img-responsive" src="./media/relevant/i_1.jpg"> </div>
         <div class="col-xs-12 col-sm-4">  <img class="img-responsive" src="./media/relevant/i_2.jpg"> </div>
         <div class="col-xs-12 col-sm-4">  <img class="img-responsive" src="./media/relevant/i_3.jpg"> </div>
</div>



</div>

<?php include ('./footer-invierno.php'); ?>  